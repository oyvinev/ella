import datetime

import pytz
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from api.tests.util import FlaskClientProxy
from vardb.datamodel import broadcast, user
from vardb.util.testdatabase import TestDatabase


def test_broadcast(test_database: TestDatabase, session: Session, client: FlaskClientProxy):
    test_database.refresh()

    # Test inital response being empty
    response = client.get("/api/v1/broadcasts/")
    assert response.get_json() == []

    # Insert a broadcast
    now = datetime.datetime.now(pytz.utc)
    b1 = broadcast.Broadcast(message="Test", active=True, date_created=now)
    session.add(b1)
    session.commit()
    response = client.get("/api/v1/broadcasts/")
    assert response.get_json() == [{"id": 1, "message": "Test", "date": now.isoformat()}]

    # Make it inactive

    b1.active = False
    session.commit()
    response = client.get("/api/v1/broadcasts/")
    assert response.get_json() == []

    # Set expiry date for user 3 days from now
    # Client logs in as testuser1 by default
    expiry_date = now + datetime.timedelta(days=3)
    client_user = session.execute(
        select(user.User).filter(user.User.username == "testuser1")
    ).scalar_one()
    client_user.password_expiry = expiry_date
    session.commit()

    response = client.get("/api/v1/broadcasts/")
    assert (
        response.get_json()[0]["message"]
        == '''Your password will expire in 2 day(s). You may change it at any time by logging out and using "Change password"'''
    )
