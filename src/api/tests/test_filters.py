import pytest
from sqlalchemy import func, select, tuple_
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.elements import BindParameter
from sqlalchemy.sql.selectable import ScalarSelect

from datalayer import filters
from vardb.datamodel import gene


def assert_results_equal(session: Session, naive, refined, N):
    expected = session.execute(select(gene.Gene).filter(naive)).all()
    actual = session.execute(select(gene.Gene).filter(refined)).all()
    assert len(expected) == N
    assert set(expected) == set(actual)


def expressions_equal(naive, refined):
    # For some reason, direct equality comparison fails, but string comparison works
    # (and it outputs the whole SQL query)
    return str(naive.expression.compile(dialect=postgresql.dialect())) == str(
        refined.expression.compile(dialect=postgresql.dialect())
    )


@pytest.mark.parametrize(
    "attr",
    (
        gene.Gene.hgnc_id,  # int
        gene.Gene.hgnc_symbol,  # str
    ),
)
def test_filter_in_uses_naive_scalar(
    session: Session, attr: InstrumentedAttribute[str] | InstrumentedAttribute[int]
):
    # Test that filters.in_ uses naive implementation for small lists (<100)
    all_values = list(session.execute(select(attr).order_by(func.random())).scalars().all())
    assert len(all_values) > 3000, "Expected more than 3000 genes in test database"

    # Should use naive implementation (right side is BindParameter) with N < 100
    N = 99
    query_values = all_values[:N]
    naive = attr.in_(query_values)
    assert isinstance(naive.right, BindParameter)
    refined = filters.in_(session, attr, query_values)  # type: ignore
    assert isinstance(refined.right, BindParameter)
    assert naive.left == refined.left == attr

    assert expressions_equal(naive, refined)
    assert_results_equal(session, naive, refined, N)

    # Should use refined implementation (right side is ScalarSelect)
    N = 101
    query_values = all_values[:N]
    naive = attr.in_(query_values)
    assert isinstance(naive.right, BindParameter)
    refined = filters.in_(session, attr, query_values)  # type: ignore
    assert isinstance(refined.right, ScalarSelect)
    assert naive.left == refined.left == attr

    assert not expressions_equal(naive, refined)
    assert_results_equal(session, naive, refined, N)


def test_filter_in_uses_naive_tuple(session: Session):
    all_values = session.execute(
        select(gene.Gene.hgnc_id, gene.Gene.hgnc_symbol).order_by(func.random())
    ).all()

    # Should use naive implementation (right side is BindParameter) with N < 100
    N = 99
    query_values = all_values[:N]
    naive = tuple_(gene.Gene.hgnc_id, gene.Gene.hgnc_symbol).in_(query_values)
    assert isinstance(naive.right, BindParameter)
    refined = filters.in_(session, (gene.Gene.hgnc_id, gene.Gene.hgnc_symbol), query_values)  # type: ignore
    assert isinstance(refined.right, BindParameter)

    assert expressions_equal(naive, refined)
    assert_results_equal(session, naive, refined, N)

    # Should use refined implementation (right side is ScalarSelect)
    N = 101
    query_values = all_values[:N]
    naive = tuple_(gene.Gene.hgnc_id, gene.Gene.hgnc_symbol).in_(query_values)
    assert isinstance(naive.right, BindParameter)
    refined = filters.in_(session, (gene.Gene.hgnc_id, gene.Gene.hgnc_symbol), query_values)  # type: ignore
    assert isinstance(refined.right, ScalarSelect)

    assert not expressions_equal(naive, refined)
    assert_results_equal(session, naive, refined, N)


def test_filter_with_query(session: Session):
    all_values = select(gene.Gene.hgnc_id).order_by(gene.Gene.hgnc_symbol).limit(100)
    naive = gene.Gene.hgnc_id.in_(all_values)
    refined = filters.in_(session, gene.Gene.hgnc_id, all_values)

    assert expressions_equal(naive, refined)
    assert_results_equal(session, naive, refined, 100)
