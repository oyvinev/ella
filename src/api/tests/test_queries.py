from sqlalchemy import distinct, func, select, tuple_
from sqlalchemy.orm.session import Session

from conftest import mock_allele_with_annotation
from datalayer import queries
from vardb.datamodel import gene
from vardb.util.testdatabase import TestDatabase


def test_distinct_inheritance_hgnc_ids_for_genepanel(session: Session):
    panel = ("Mendeliome", "v1.0.0")

    gp_hgnc_ids = (
        session.execute(
            select(gene.Gene.hgnc_id.distinct())
            .join(gene.Transcript)
            .join(gene.genepanel_transcript)
            .join(gene.Genepanel)
            .filter(gene.Genepanel.name == panel[0], gene.Genepanel.version == panel[1])
        )
        .scalars()
        .all()
    )

    # Make sure we actually have some genes in the genepanel
    assert gp_hgnc_ids, f"Genepanel {panel} has no genes"

    all_inheritances = (
        session.execute(select(gene.genepanel_transcript.c.inheritance.distinct())).scalars().all()
    )
    hgnc_ids_found = set()
    for inheritance in all_inheritances:
        hgnc_ids: set[int] = set(
            session.execute(
                queries.distinct_inheritance_hgnc_ids_for_genepanel(inheritance, panel[0], panel[1])
            )
            .scalars()
            .all()
        )

        # Make sure all genes are actually part of input genepanel
        assert session.execute(
            select((func.count(distinct(gene.Transcript.gene_id))))
            .join(gene.Genepanel.transcripts)
            .filter(
                tuple_(gene.Genepanel.name, gene.Genepanel.version) == panel,
                gene.Transcript.gene_id.in_(hgnc_ids),
            )
        ).scalar_one() == len(hgnc_ids)

        # Test that AD matches only has 'AD' inheritance
        inheritances = (
            session.execute(
                select(gene.genepanel_transcript.c.inheritance)
                .join(gene.Transcript)
                .filter(
                    tuple_(
                        gene.genepanel_transcript.c.genepanel_name,
                        gene.genepanel_transcript.c.genepanel_version,
                    )
                    == panel,
                    gene.Transcript.gene_id.in_(hgnc_ids),
                )
            )
            .scalars()
            .all()
        )

        if set(inheritances):
            assert set(inheritances) == {
                inheritance
            }, f"Found inheritance for hgnc ids not equal to {inheritance}: {inheritances}"
        else:
            assert (
                len(hgnc_ids) == 0
            ), f"Did not find inheritance {inheritance} in genepanel, but query returned hgnc_ids {hgnc_ids}"

        # Test opposite case: non-AD genes has non-AD inheritance
        inheritances = (
            session.execute(
                select(gene.genepanel_transcript.c.inheritance)
                .join(gene.Transcript)
                .filter(
                    tuple_(
                        gene.genepanel_transcript.c.genepanel_name,
                        gene.genepanel_transcript.c.genepanel_version,
                    )
                    == panel,
                    ~gene.Transcript.gene_id.in_(hgnc_ids),
                )
            )
            .scalars()
            .all()
        )
        assert {inheritance} & set(
            inheritances
        ) == set(), f"Found other genes with inheritance {inheritance} than returned from query"

        hgnc_ids_found |= hgnc_ids

    assert hgnc_ids_found == set(gp_hgnc_ids), "All genes should be found in inheritance query"


def test_annotation_transcripts_genepanel(session: Session, test_database: TestDatabase):
    test_database.refresh()

    def insert_data():
        g1 = gene.Gene(hgnc_id=1, hgnc_symbol="GENE1")
        g2 = gene.Gene(hgnc_id=2, hgnc_symbol="GENE2")
        g3 = gene.Gene(hgnc_id=3, hgnc_symbol="GENE3")
        transcript_base = {
            "type": "RefSeq",
            "genome_reference": "123",
            "chromosome": "123",
            "tags": None,
            "tx_start": 123,
            "tx_end": 123,
            "strand": "+",
            "cds_start": 123,
            "cds_end": 123,
            "exon_starts": [123, 321],
            "exon_ends": [123, 321],
        }

        t1 = gene.Transcript(gene=g1, transcript_name="NM_1.1", **transcript_base)
        session.add(t1)

        t21 = gene.Transcript(gene=g2, transcript_name="NM_2.1", **transcript_base)
        session.add(t21)

        t22 = gene.Transcript(gene=g2, transcript_name="NM_2.2", **transcript_base)
        session.add(t22)

        t3 = gene.Transcript(gene=g3, transcript_name="NM_3.1", **transcript_base)
        session.add(t3)

        genepanel1 = gene.Genepanel(name="testpanel1", version="v01", genome_reference="GRCh37")
        session.add(genepanel1)
        session.flush()
        for tx in [t1, t21]:
            session.execute(
                gene.genepanel_transcript.insert(),
                {
                    "transcript_id": tx.id,
                    "genepanel_name": genepanel1.name,
                    "genepanel_version": genepanel1.version,
                    "inheritance": "AD/AR",
                },
            )

        genepanel2 = gene.Genepanel(name="testpanel2", version="v01", genome_reference="GRCh37")
        session.add(genepanel2)
        session.flush()
        for tx in [t22, t3]:
            session.execute(
                gene.genepanel_transcript.insert(),
                {
                    "transcript_id": tx.id,
                    "genepanel_name": genepanel2.name,
                    "genepanel_version": genepanel2.version,
                    "inheritance": "AD/AR",
                },
            )
        session.flush()

        a1, _ = mock_allele_with_annotation(
            session,
            annotations={
                "transcripts": [
                    {"transcript": "NM_1.1", "hgnc_id": 1},  # In genepanel
                    {"transcript": "NM_1", "hgnc_id": 1},  # In genepanel, no version
                    {
                        "transcript": "NM_2.2",
                        "hgnc_id": 2,
                    },  # In two genepanels, different version in one
                    {"transcript": "NM_NOT_IN_PANEL.1", "hgnc_id": 1},  # Not in genepanel
                    {"transcript": "NM_NOT_IN_PANEL", "hgnc_id": 2},  # Not in genepanel, no version
                ]
            },
        )

        a2, _ = mock_allele_with_annotation(
            session,
            annotations={
                "transcripts": [
                    {"transcript": "NM_3.1", "hgnc_id": 3},  # In one genepanel
                    {"transcript": "NM_3", "hgnc_id": 3},  # In one genepanel, no version
                    {
                        "transcript": "NM_2.2",
                        "hgnc_id": 2,
                    },  # In two genepanels, different version in one
                    {"transcript": "NM_NOT_IN_PANEL.1", "hgnc_id": 1},  # Not in any genepanel
                    {
                        "transcript": "NM_NOT_IN_PANEL",
                        "hgnc_id": 2,
                    },  # Not in any genepanel, no version
                ]
            },
        )

        a3, _ = mock_allele_with_annotation(
            session,
            annotations={
                "transcripts": [
                    {"transcript": "NM_1.2", "hgnc_id": 1},
                    {"transcript": "NM_1.3", "hgnc_id": 1},
                    {"transcript": "NM_2.1", "hgnc_id": 2},
                    {"transcript": "NM_2.2", "hgnc_id": 2},
                    {"transcript": "NM_3.1_sometext", "hgnc_id": 3},
                    {"transcript": "NM_3.2", "hgnc_id": 3},
                    {"transcript": "NM_3.3_sometext", "hgnc_id": 3},
                    {"transcript": "NM_3", "hgnc_id": 3},
                ]
            },
        )

        return a1, a2, a3

    a1, a2, a3 = insert_data()
    session.flush()

    annotation_transcripts_genepanel = queries.annotation_transcripts_genepanel(
        session, [("testpanel1", "v01"), ("testpanel2", "v01")]
    ).subquery()

    result = session.execute(
        select(
            annotation_transcripts_genepanel.c.allele_id,
            annotation_transcripts_genepanel.c.name,
            annotation_transcripts_genepanel.c.version,
            annotation_transcripts_genepanel.c.annotation_transcript,
            annotation_transcripts_genepanel.c.genepanel_transcript,
        )
    ).all()

    passes = [
        # allele_id, panel, annotation, genepanel
        (a1.id, "testpanel1", "v01", "NM_1.1", "NM_1.1"),
        (a1.id, "testpanel1", "v01", "NM_2.2", "NM_2.1"),
        (a1.id, "testpanel2", "v01", "NM_2.2", "NM_2.2"),
        (a2.id, "testpanel1", "v01", "NM_2.2", "NM_2.1"),
        (a2.id, "testpanel2", "v01", "NM_2.2", "NM_2.2"),
        (a2.id, "testpanel2", "v01", "NM_3.1", "NM_3.1"),
        (a3.id, "testpanel1", "v01", "NM_1.3", "NM_1.1"),
        (a3.id, "testpanel1", "v01", "NM_2.1", "NM_2.1"),
        (a3.id, "testpanel2", "v01", "NM_2.2", "NM_2.2"),
        (a3.id, "testpanel2", "v01", "NM_3.3_sometext", "NM_3.1"),
    ]

    assert set(result) == set(passes)

    session.rollback()
