from sqlalchemy import select
from sqlalchemy.orm import Session

from api import schemas
from api.schemas.pydantic.v1 import validate_output
from api.schemas.pydantic.v1.resources import FilterConfigResponse
from api.util.util import authenticate
from api.v1.resource import LogRequestResource
from vardb.datamodel.sample import FilterConfig, UserGroupFilterConfig
from vardb.datamodel.user import User


class FilterconfigResource(LogRequestResource):
    @authenticate()
    @validate_output(FilterConfigResponse)
    def get(self, session: Session, filterconfig_id: int, user: User):
        filterconfig = session.execute(
            select(FilterConfig)
            .join(UserGroupFilterConfig)
            .filter(
                FilterConfig.id == filterconfig_id,
                UserGroupFilterConfig.usergroup_id == user.group_id,
            )
        ).scalar_one()
        return schemas.FilterConfigSchema().dump(filterconfig).data
