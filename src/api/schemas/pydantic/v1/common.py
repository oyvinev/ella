from pydantic import ConfigDict

from api.schemas.pydantic.v1 import BaseModel


class Comment(BaseModel):
    comment: str


class SearchFilter(BaseModel):
    search_string: str


class GenericID(BaseModel):
    id: int

    model_config = ConfigDict(extra="allow")
