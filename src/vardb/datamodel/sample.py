#!/usr/bin/env python
"""varDB datamodel classes for entities that relate to samples."""

from datetime import datetime
from typing import TYPE_CHECKING, Literal, Optional

import pytz
from sqlalchemy import Boolean, DateTime, Enum, FetchedValue, ForeignKey, Integer, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, Index

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.gene import Genepanel
    from vardb.datamodel.workflow import AnalysisInterpretation


class Sample(Base):
    """Represents a sample (aka one sequencing run of 1 biological sample)

    Can represent samples from two types of technologies, Sanger and HTS.

    Note: there can be multiple samples with same name in database, and they might differ in genotypes.
    This happens when multiple analyses, using the same sample data in pipeline, is imported.
    They can have been run on different regions.
    """

    __tablename__ = "sample"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    identifier: Mapped[str]
    analysis_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="CASCADE"), nullable=False
    )
    analysis: Mapped["Analysis"] = relationship("Analysis", back_populates="samples")
    sample_type: Mapped[Literal["HTS"] | Literal["Sanger"]] = mapped_column(
        Enum("HTS", "Sanger", name="sample_type"), nullable=False
    )
    proband: Mapped[bool]
    family_id: Mapped[str | None]
    father_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("sample.id")
    )  # Set on proband
    mother_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("sample.id")
    )  # Set on proband
    sibling_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("sample.id")
    )  # Set for siblings pointing to proband (i.e. _not_ on proband)
    sex: Mapped[str | None] = mapped_column(
        Enum("Male", "Female", name="sample_sex")
    )  # Can be unknown
    affected: Mapped[bool]
    date_deposited: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )

    __table_args__ = (Index("ix_sampleidentifier", "identifier"),)

    def __repr__(self):
        return "<Sample('%s', '%s')>" % (self.identifier, self.sample_type)


class Analysis(Base):
    """Represents a bioinformatical pipeline analysis

    An Analysis will have produced variant descriptions (e.g. VCF),
    that are an object for Interpretation.
    """

    __tablename__ = "analysis"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String(), nullable=False, unique=True)
    genepanel_name: Mapped[str | None] = mapped_column(String)  # TODO: non-nullable?
    genepanel_version: Mapped[str | None] = mapped_column(String)  # TODO: non-nullable?
    genepanel: Mapped[Optional["Genepanel"]] = relationship("Genepanel", uselist=False)
    warnings: Mapped[str | None]
    report: Mapped[str | None]
    date_requested: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    date_deposited: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    interpretations: Mapped[list["AnalysisInterpretation"]] = relationship(
        "AnalysisInterpretation", order_by="AnalysisInterpretation.id", viewonly=True
    )
    properties: Mapped[dict | None] = mapped_column(
        JSONMutableDict.as_mutable(JSONB)
    )  # Holds commments, tags etc
    __table_args__ = (
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    samples: Mapped[list["Sample"]] = relationship()

    def __repr__(self):
        return "<Analysis('%s, %s, %s')>" % (
            self.samples,
            self.genepanel_name,
            self.genepanel_version,
        )


class FilterConfig(Base):

    """
    Represents a configuration of an analysis,
    e.g. ACMG configuration and filter configuration.

    Note that one analysis can utilise several AnalysisConfigs as part of
    a workflow/interpretation.
    """

    __tablename__ = "filterconfig"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str]
    filterconfig: Mapped[dict] = mapped_column(JSONMutableDict.as_mutable(JSONB), nullable=False)
    schema_version: Mapped[int] = mapped_column(
        Integer, nullable=False, server_default=FetchedValue()
    )
    requirements: Mapped[dict] = mapped_column(JSONB, nullable=False, default=[])
    date_superceeded: Mapped[datetime | None] = mapped_column(
        "date_superceeded", DateTime(timezone=True)
    )
    previous_filterconfig_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("filterconfig.id")
    )
    active: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)

    def __repr__(self):
        return "<FilterConfig({}, {})>".format(self.id, self.name)


class UserGroupFilterConfig(Base):
    __tablename__ = "usergroupfilterconfig"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    usergroup_id: Mapped[int] = mapped_column(Integer, ForeignKey("usergroup.id"), nullable=False)
    filterconfig_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("filterconfig.id"), nullable=False
    )
    order: Mapped[int] = mapped_column(Integer, nullable=False)


Index(
    "uq_filterconfig_name_active_unique",
    FilterConfig.name,
    postgresql_where=FilterConfig.active.is_(True),
    unique=True,
)

Index(
    "uq_usergroupfilterconfig_unique",
    UserGroupFilterConfig.usergroup_id,
    UserGroupFilterConfig.filterconfig_id,
    unique=True,
)
