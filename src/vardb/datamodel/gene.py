"""varDB datamodel classes for Gene and Transcript"""
from datetime import datetime
from typing import TYPE_CHECKING, Optional

import pytz
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Index,
    Integer,
    String,
    Table,
    Text,
    func,
)
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, UniqueConstraint

from vardb.datamodel import Base

if TYPE_CHECKING:
    from vardb.datamodel.user import User


class Gene(Base):
    """Represents a gene abstraction"""

    __tablename__ = "gene"

    hgnc_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    hgnc_symbol: Mapped[str] = mapped_column(String(), nullable=False)

    __table_args__ = (
        Index(
            "ix_gene_hgnc_symbol",
            func.lower(hgnc_symbol),
            unique=True,
            postgresql_ops={"data": "text_pattern_ops"},
        ),
    )

    def __repr__(self):
        return "<Gene(%d, '%s')>" % (self.hgnc_id, self.hgnc_symbol)


class Transcript(Base):
    """Represents a gene transcript"""

    __tablename__ = "transcript"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    gene_id: Mapped[int] = mapped_column(Integer, ForeignKey("gene.hgnc_id"), nullable=False)
    gene: Mapped["Gene"] = relationship("Gene", lazy="joined")
    transcript_name: Mapped[str] = mapped_column(String(), unique=True, nullable=False)
    type: Mapped[str] = mapped_column(
        Enum("RefSeq", "Ensembl", "LRG", name="transcript_type"), nullable=False
    )
    tags: Mapped[list[str] | None] = mapped_column(ARRAY(Text), nullable=True)
    genome_reference: Mapped[str]
    chromosome: Mapped[str]
    tx_start: Mapped[int]
    tx_end: Mapped[int]
    strand: Mapped[str] = mapped_column(String(1), nullable=False)
    cds_start: Mapped[int | None]
    cds_end: Mapped[int | None]
    exon_starts: Mapped[list[int]] = mapped_column(ARRAY(Integer), nullable=False)
    exon_ends: Mapped[list[int]] = mapped_column(ARRAY(Integer), nullable=False)

    def __repr__(self):
        return "<Transcript('%s','%s', '%s', '%s', '%s', '%s')>" % (
            self.gene,
            self.transcript_name,
            self.chromosome,
            self.tx_start,
            self.tx_end,
            self.strand,
        )

    def __str__(self):
        return "%s, %s, %s, %s, %s, %s" % (
            self.gene,
            self.transcript_name,
            self.chromosome,
            self.tx_start,
            self.tx_end,
            self.strand,
        )


# Association table uses ForeignKeyContraint for referencing composite primary key in gene panel.
genepanel_transcript = Table(
    "genepanel_transcript",
    Base.metadata,
    Column("genepanel_name", nullable=False),
    Column("genepanel_version", nullable=False),
    Column("transcript_id", Integer, ForeignKey("transcript.id"), index=True, nullable=False),
    Column("inheritance", Text(), nullable=False),
    ForeignKeyConstraint(
        ["genepanel_name", "genepanel_version"],
        ["genepanel.name", "genepanel.version"],
        ondelete="CASCADE",
    ),
)


class Phenotype(Base):
    """Represents a gene phenotype"""

    __tablename__ = "phenotype"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)

    gene_id: Mapped[int] = mapped_column(Integer, ForeignKey("gene.hgnc_id"), nullable=False)
    gene: Mapped["Gene"] = relationship("Gene", lazy="joined")

    description: Mapped[str]
    inheritance: Mapped[str]
    omim_id: Mapped[int | None]

    __table_args__ = (UniqueConstraint("gene_id", "description", "inheritance"),)

    def __repr__(self):
        return "<Phenotype('%s')>" % self.description[:20]


genepanel_phenotype = Table(
    "genepanel_phenotype",
    Base.metadata,
    Column("genepanel_name", nullable=False),
    Column("genepanel_version", nullable=False),
    Column("phenotype_id", Integer, ForeignKey("phenotype.id"), nullable=False),
    ForeignKeyConstraint(
        ["genepanel_name", "genepanel_version"],
        ["genepanel.name", "genepanel.version"],
        ondelete="CASCADE",
    ),
)


class Genepanel(Base):
    """Represents a gene panel"""

    __tablename__ = "genepanel"

    name: Mapped[str] = mapped_column(String(), primary_key=True)
    version: Mapped[str] = mapped_column(String(), primary_key=True)
    genome_reference: Mapped[str]
    official: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    user_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("user.id"), nullable=True)
    user: Mapped[Optional["User"]] = relationship("User", uselist=False)
    transcripts: Mapped[list["Transcript"]] = relationship(
        "Transcript", secondary=genepanel_transcript
    )
    phenotypes: Mapped[list["Phenotype"]] = relationship("Phenotype", secondary=genepanel_phenotype)

    def __repr__(self):
        return f"<Genepanel('{self.name}','{self.version}', '{self.genome_reference}')>"

    def __str__(self):
        return "_".join((self.name, self.version, self.genome_reference))
