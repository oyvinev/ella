#!/usr/bin/env python

from datetime import datetime
from typing import TYPE_CHECKING, Literal, Optional

import pytz
from sqlalchemy import (
    DateTime,
    Enum,
    ForeignKey,
    ForeignKeyConstraint,
    Index,
    Integer,
    UniqueConstraint,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import Mapped, Session, mapped_column, relationship

from vardb.datamodel import Base
from vardb.datamodel.allele import Allele
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.gene import Genepanel
    from vardb.datamodel.sample import Analysis
    from vardb.datamodel.user import User


class InterpretationMixin(object):
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    genepanel_name: Mapped[str]
    genepanel_version: Mapped[str]
    user_state: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    state: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    status: Mapped[str] = mapped_column(
        Enum("Not started", "Ongoing", "Done", name="interpretation_status"),
        default="Not started",
        nullable=False,
    )
    finalized: Mapped[bool | None]
    date_last_update: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )

    @declared_attr
    def user_id(cls) -> Mapped[int | None]:
        return mapped_column(Integer, ForeignKey("user.id"))

    @declared_attr
    def user(cls) -> Mapped[Optional["User"]]:
        return relationship("User")

    @declared_attr
    def genepanel(cls) -> Mapped["Genepanel"]:
        return relationship("Genepanel", uselist=False)

    @declared_attr
    def __table_args__(cls):
        return (
            ForeignKeyConstraint(
                ["genepanel_name", "genepanel_version"], ["genepanel.name", "genepanel.version"]
            ),
        )

    @classmethod
    def create_next(cls, old):
        next_obj = cls()
        for attr in cls.__next_attrs__:
            setattr(next_obj, attr, getattr(old, attr))
        return next_obj


class InterpretationSnapshotMixin(object):
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    filtered: Mapped[str | None] = mapped_column(
        Enum(
            "CLASSIFICATION",
            "FREQUENCY",
            "REGION",
            "POLYPYRIMIDINE",
            "GENE",
            "QUALITY",
            "CONSEQUENCE",
            "SEGREGATION",
            "INHERITANCEMODEL",
            name="interpretationsnapshot_filtered",
        )
    )  # If the allele was filtered, this describes which type of filtering

    @declared_attr
    def annotation_id(cls) -> Mapped[int | None]:
        return mapped_column(
            Integer, ForeignKey("annotation.id"), nullable=True
        )  # None for an excluded allele

    @declared_attr
    def customannotation_id(cls) -> Mapped[int | None]:
        return mapped_column(Integer, ForeignKey("customannotation.id"))

    @declared_attr
    def alleleassessment_id(cls) -> Mapped[int | None]:
        return mapped_column(Integer, ForeignKey("alleleassessment.id"))

    @declared_attr
    def allelereport_id(cls) -> Mapped[int | None]:
        return mapped_column(Integer, ForeignKey("allelereport.id"))


class AnalysisInterpretation(Base, InterpretationMixin):
    """Represents an Interpretation by a labengineer

    This corresponds to one interpretation 'round' of an analysis.
    The table stores both normal state and user-specific state for each round,
    while keeping a history of the state upon update.
    """

    __tablename__ = "analysisinterpretation"

    __next_attrs__ = ["analysis_id", "state", "genepanel_name", "genepanel_version"]

    analysis_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="CASCADE"), nullable=False
    )
    analysis: Mapped["Analysis"] = relationship("Analysis", uselist=False)
    workflow_status: Mapped[str] = mapped_column(
        Enum(
            "Not ready",
            "Interpretation",
            "Review",
            "Medical review",
            name="analysisinterpretation_workflow_status",
        ),
        default="Interpretation",
        nullable=False,
    )

    def __repr__(self):
        return "<Interpretation('{}', '{}')>".format(str(self.analysis_id), self.status)


Index(
    "ix_analysisinterpretation_analysisid_ongoing_unique",
    AnalysisInterpretation.analysis_id,
    postgresql_where=(AnalysisInterpretation.status.in_(["Ongoing", "Not started"])),
    unique=True,
)


class AnalysisInterpretationSnapshot(Base, InterpretationSnapshotMixin):
    """
    Represents a snapshot of a allele interpretation,
    at the time when it was marked as 'Done'.
    It's logging all relevant context for the interpretation.
    """

    __tablename__ = "analysisinterpretationsnapshot"

    analysisinterpretation_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("analysisinterpretation.id", ondelete="CASCADE"), nullable=False
    )
    analysisinterpretation: Mapped["AnalysisInterpretation"] = relationship(
        "AnalysisInterpretation", backref="snapshots"
    )
    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    __table_args__ = (UniqueConstraint("analysisinterpretation_id", "allele_id"),)


class AlleleInterpretation(Base, InterpretationMixin):
    """Represents an interpretation of an allele by a labengineer

    This corresponds to one interpretation 'round' of an allele.
    The table stores both normal state and user-specific state for each round,
    while keeping a history of the state upon update.
    """

    __tablename__ = "alleleinterpretation"

    __next_attrs__ = ["allele_id", "state", "genepanel_name", "genepanel_version"]

    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    allele: Mapped["Allele"] = relationship("Allele", uselist=False)
    workflow_status: Mapped[Literal["Interpretation"] | Literal["Review"]] = mapped_column(
        Enum("Interpretation", "Review", name="alleleinterpretation_workflow_status"),
        default="Interpretation",
        nullable=False,
    )

    @classmethod
    def get_or_create(cls, session: Session, defaults=None, allele_id=None, **kwargs):
        "Override to check that allele is not CNV."
        caller_type = session.execute(
            select(Allele.caller_type).filter(Allele.id == allele_id)
        ).scalar_one()

        if caller_type.lower() == "cnv":
            raise NotImplementedError("Standalone CNV variants not yet supported")

        return super(Base, cls).get_or_create(
            session, defaults=defaults, allele_id=allele_id, **kwargs
        )

    def __repr__(self):
        return "<AlleleInterpretation('{}', '{}')>".format(str(self.allele_id), self.status)


Index(
    "ix_alleleinterpretation_alleleid_ongoing_unique",
    AlleleInterpretation.allele_id,
    postgresql_where=(AlleleInterpretation.status.in_(["Ongoing", "Not started"])),
    unique=True,
)


class AlleleInterpretationSnapshot(Base, InterpretationSnapshotMixin):
    """
    Represents a snapshot of a allele interpretation,
    at the time when it was marked as 'Done'.
    It's logging all relevant context for the interpretation.
    """

    __tablename__ = "alleleinterpretationsnapshot"

    alleleinterpretation_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("alleleinterpretation.id", ondelete="CASCADE"), nullable=False
    )
    alleleinterpretation: Mapped["AlleleInterpretation"] = relationship(
        "AlleleInterpretation", backref="snapshots"
    )
    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    __table_args__ = (UniqueConstraint("alleleinterpretation_id", "allele_id"),)


class InterpretationStateHistory(Base):
    """
    Holds the history of the state for the interpretations.
    Every time the [allele|analysis]interpretation state is updated (i.e. when user saves),
    it's copied into this table.
    """

    __tablename__ = "interpretationstatehistory"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    alleleinterpretation_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("alleleinterpretation.id", ondelete="CASCADE")
    )
    analysisinterpretation_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysisinterpretation.id", ondelete="CASCADE")
    )
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    state: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), nullable=False)


class InterpretationLog(Base):
    __tablename__ = "interpretationlog"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    alleleinterpretation_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("alleleinterpretation.id", ondelete="CASCADE")
    )
    analysisinterpretation_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysisinterpretation.id", ondelete="CASCADE")
    )
    user_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("user.id")
    )  # Can be null if created by system
    user: Mapped[Optional["User"]] = relationship("User")
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    message: Mapped[str | None]
    priority: Mapped[int | None]
    review_comment: Mapped[str | None]
    warning_cleared: Mapped[bool | None]
    alleleassessment_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("alleleassessment.id")
    )
    allelereport_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("allelereport.id"))
