"""Various column indexes

Revision ID: 11ec14c56ae2
Revises: 4c2844fef850
Create Date: 2023-10-27 07:26:16.923178

"""

# revision identifiers, used by Alembic.
revision = "11ec14c56ae2"
down_revision = "4c2844fef850"
branch_labels = None
depends_on = None

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.create_index(
        op.f("ix_genepanel_transcript_transcript_id"),
        "genepanel_transcript",
        ["transcript_id"],
        unique=False,
    )
    op.create_index(op.f("ix_genotype_allele_id"), "genotype", ["allele_id"], unique=False)
    op.create_index(
        op.f("ix_genotype_secondallele_id"), "genotype", ["secondallele_id"], unique=False
    )

    # This should already exist, but doesn't appear to be the case
    # Add if_not_exists=True to avoid error
    op.create_index(
        op.f("ix_genotypesampledata_sample_id"),
        "genotypesampledata",
        ["sample_id"],
        unique=False,
        if_not_exists=True,
    )
    op.create_index(
        op.f("ix_genotypesampledata_secondallele"),
        "genotypesampledata",
        ["secondallele"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_genotype_secondallele_id"), table_name="genotype")
    op.drop_index(op.f("ix_genotype_allele_id"), table_name="genotype")
    op.drop_index(op.f("ix_genepanel_transcript_transcript_id"), table_name="genepanel_transcript")

    # Do not drop ix_genotypesampledata_sample_id, as this should already be here before this migration
