from datetime import datetime
from typing import TYPE_CHECKING, Optional

import pytz
from sqlalchemy import DateTime, Enum, ForeignKey, ForeignKeyConstraint, Integer, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import CheckConstraint

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.gene import Genepanel
    from vardb.datamodel.user import User


class AnnotationJob(Base):
    """
    Represents an annotation job submitted for annotation.

    This will be picked up by the annotation service polling thread, and sent to the annotation server.
    The feedback from the annotation server will be used to update the status and message-fields
    in the table.

    """

    __tablename__ = "annotationjob"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    task_id: Mapped[str | None] = mapped_column(String, default="")

    status: Mapped[str] = mapped_column(
        Enum(
            "SUBMITTED",
            "RUNNING",
            "ANNOTATED",
            "CANCELLED",
            "DONE",
            "FAILED (SUBMISSION)",
            "FAILED (ANNOTATION)",
            "FAILED (DEPOSIT)",
            "FAILED (PROCESSING)",
            name="job_status",
        ),
        default="SUBMITTED",
        nullable=False,
    )
    status_history: Mapped[dict | None] = mapped_column(
        JSONMutableDict.as_mutable(JSONB), default={}
    )
    mode: Mapped[str | None] = mapped_column(
        Enum("Analysis", "Variants", "Single variant", name="mode")
    )
    sample_id: Mapped[str | None]
    data: Mapped[str | None]
    message: Mapped[str | None] = mapped_column(String, default="")
    user_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("user.id"))
    user: Mapped[Optional["User"]] = relationship("User", uselist=False)
    date_submitted: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    date_last_update: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    genepanel_name: Mapped[str | None] = mapped_column(String)
    genepanel_version: Mapped[str | None] = mapped_column(String)
    genepanel: Mapped[Optional["Genepanel"]] = relationship("Genepanel", uselist=False)

    properties: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB))

    __table_args__ = (
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
        CheckConstraint("NOT(data IS NULL AND sample_id IS NULL)"),
        CheckConstraint("data IS NULL OR sample_id IS NULL"),
    )

    def __repr__(self):
        return "<AnnotationJob('{}', '{}', '{}')".format(str(self.id), self.task_id, self.status)
