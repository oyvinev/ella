import datetime

import pytz
from sqlalchemy import Boolean, DateTime, Integer, String
from sqlalchemy.orm import mapped_column

from vardb.datamodel import Base


class Broadcast(Base):
    """Broadcast message"""

    __tablename__ = "broadcast"

    id = mapped_column(Integer, primary_key=True)
    date_created = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.datetime.now(pytz.utc)
    )
    message = mapped_column(String, nullable=False)
    active = mapped_column(Boolean, nullable=False)
