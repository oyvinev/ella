from datetime import datetime
from typing import TYPE_CHECKING, Optional

import pytz
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Table,
    UniqueConstraint,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.gene import Genepanel
# Links user groups to genepanels
UserGroupGenepanel = Table(
    "usergroupgenepanel",
    Base.metadata,
    Column("usergroup_id", Integer, ForeignKey("usergroup.id")),
    Column("genepanel_name", String, nullable=False),
    Column("genepanel_version", String, nullable=False),
    ForeignKeyConstraint(
        ["genepanel_name", "genepanel_version"], ["genepanel.name", "genepanel.version"]
    ),
)


class User(Base):
    """Represents a user."""

    __tablename__ = "user"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    username: Mapped[str] = mapped_column(String(), nullable=False, unique=True)
    first_name: Mapped[str]
    last_name: Mapped[str]
    email: Mapped[str | None]
    group_id: Mapped[int] = mapped_column(Integer, ForeignKey("usergroup.id"), nullable=False)
    group: Mapped["UserGroup"] = relationship("UserGroup", uselist=False, backref="users")
    password: Mapped[str]
    password_expiry: Mapped[datetime] = mapped_column(DateTime(timezone=True), nullable=False)
    active: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)
    incorrect_logins: Mapped[int] = mapped_column(Integer, default=0, nullable=False)
    config: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})


class UserGroup(Base):
    __tablename__ = "usergroup"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String(), nullable=False, unique=True)
    genepanels: Mapped[list["Genepanel"]] = relationship("Genepanel", secondary=UserGroupGenepanel)
    config: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    default_import_genepanel_name: Mapped[str | None] = mapped_column(String)
    default_import_genepanel_version: Mapped[str | None] = mapped_column(String)
    default_import_genepanel: Mapped[Optional["Genepanel"]] = relationship(
        "Genepanel", uselist=False
    )

    __table_args__ = (
        ForeignKeyConstraint(
            [default_import_genepanel_name, default_import_genepanel_version],
            ["genepanel.name", "genepanel.version"],
        ),
    )


# Represents relationship of what groups are allowed to import data into which groups
UserGroupImport = Table(
    "usergroupimport",
    Base.metadata,
    Column("usergroup_id", Integer, ForeignKey("usergroup.id"), nullable=False),
    Column("usergroupimport_id", Integer, ForeignKey("usergroup.id"), nullable=False),
    UniqueConstraint("usergroup_id", "usergroupimport_id"),
)


class UserSession(Base):
    """Represents a user session"""

    __tablename__ = "usersession"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    user_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("user.id")
    )  # TODO: non-nullable?
    user: Mapped[Optional["User"]] = relationship("User")
    token: Mapped[str] = mapped_column(String(), nullable=False, unique=True)
    issued: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc), nullable=False
    )
    lastactivity: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc), nullable=False
    )
    expires: Mapped[datetime] = mapped_column(DateTime(timezone=True), nullable=False)
    logged_out: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))


class UserOldPassword(Base):
    __tablename__ = "useroldpassword"
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    user_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("user.id"))
    password: Mapped[str]
    expired: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc), nullable=False
    )
