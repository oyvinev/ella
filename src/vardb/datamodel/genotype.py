"""vardb datamodel Genotype class"""
from typing import TYPE_CHECKING, Optional

from sqlalchemy import Boolean, CheckConstraint, Enum, ForeignKey, Integer, SmallInteger
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.allele import Allele
    from vardb.datamodel.sample import Sample


class Genotype(Base):
    """
    Represent an observed diploid genotype for proband (i.e. an instance of a pair of alleles.)

    We only store the proband's variants in the database. Related metadata for the genotype for whole
    family is stored in genotypemetadata table.

    Genotype rows are only given for proband samples.
    """

    __tablename__ = "genotype"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    # Shortcut to get both alleles
    alleles: Mapped[list["Allele"]] = relationship(
        "Allele",
        primaryjoin="or_(Allele.id==Genotype.allele_id, " "Allele.id==Genotype.secondallele_id)",
        uselist=True,
        viewonly=True,
    )
    allele_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("allele.id"), index=True, nullable=False
    )
    secondallele_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("allele.id"), index=True
    )
    allele: Mapped["Allele"] = relationship("Allele", primaryjoin=("Genotype.allele_id==Allele.id"))
    secondallele: Mapped[Optional["Allele"]] = relationship(
        "Allele", primaryjoin=("Genotype.secondallele_id==Allele.id")
    )  # None, unless heterozygous nonreference
    sample_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("sample.id", ondelete="CASCADE"), index=True, nullable=False
    )  # Proband's sample id, set for optimization only
    sample: Mapped["Sample"] = relationship("Sample", backref="genotypes")
    filter_status: Mapped[str | None]
    variant_quality: Mapped[int | None]

    def __repr__(self):
        return "<Genotype('%s','%s', '%s')>" % (self.allele, self.secondallele, self.sample)


class GenotypeSampleData(Base):
    """
    Provides information for many samples in relation to one (proband) genotype.

    For biallelic genotypes (when genotype.secondallele_id is not null), there will be two entries
    per sample.

    Represents a direct translation of a (decomposed) vcf per-sample data.

    Note that we only store actual variants for proband samples,
    so this table represents metadata for other samples with regards
    to the proband's variants.

                        +-----------------------------+
                        |----------------------+      |
                        +---------------|      |      |
                        |      +---+  +---+  +-+-+  +-+-+
     GenotypeSampleData +------+GSD|  |GSD|  |GSD|  |GSD|
                        |      +-+-+  +-+-+  +-+-+  +-+-+
                        |        |      |      |      |
      +--------+   +----v----+   |      |      |      |
      | Allele +^--+Genotype |   |      |      |      |
      +--------+   +----^----+   |      |      |      |
                        |        |      |      |      |
                        |      +-v-+  +-v-+  +-v-+  +-v-+
           Samples      +------+ P |  | F |  | M |  | S |
                               +---+  +---+  +---+  +---+



    Some important specifics:
     - secondallele: Whether current entry applies to the secondallele_id in connected genotype
     - type: Given in relation to the _proband's_ variant. E.g. if a father sample is
       given as 'Reference' in regards to a proband's variant, it could also have another variant
       (that is not stored) at this site. This example would equal a '0/.' GT in the vcf.
     - multiallelic: Indicates whether this site is multiallelic in relation to the sample_id. In other words whether there's
       another variant in this location for this sample if type is given as 'Heterozygous' or 'Reference'.
       Will be set for vcf GT of '1/.' or '0/.'.
    """

    __tablename__ = "genotypesampledata"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    # Family samples will connect to proband's genotype_ids
    genotype_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("genotype.id", ondelete="CASCADE"), index=True, nullable=False
    )
    genotype: Mapped["Genotype"] = relationship(Genotype, backref="genotypesampledata")
    secondallele: Mapped[bool] = mapped_column(
        Boolean, index=True
    )  # Whether this entry applies to the secondallele_id in genotype
    multiallelic: Mapped[
        bool
    ]  # Whether the site is multiallelic for _this sample_. I.e the sample has another variant than the proband's one.
    type: Mapped[str] = mapped_column(
        Enum(
            "Homozygous", "Heterozygous", "Reference", "No coverage", name="genotypesampledata_type"
        ),
        nullable=False,
    )  # The sample's genotype in relation to the proband's variant.
    sample_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("sample.id", ondelete="CASCADE"), index=True, nullable=False
    )
    genotype_quality: Mapped[int | None] = mapped_column(SmallInteger)  # GQ
    sequencing_depth: Mapped[int | None] = mapped_column(SmallInteger)  # DP
    allele_ratio: Mapped[float | None]  # Read ratio for the allele
    genotype_likelihood: Mapped[list[int] | None] = mapped_column(
        ARRAY(Integer),
        CheckConstraint("array_position(genotype_likelihood, NULL) is NULL"),
    )  # PL Phred scale score for each type: [(0,0), (0,1), (1,1)]
    allele_depth: Mapped[dict | None] = mapped_column(
        JSONMutableDict.as_mutable(JSONB), default={}
    )  # AD {'REF': 0, 'A': 23, 'G': 32}
    copy_number: Mapped[int | None] = mapped_column(SmallInteger)  # CN
