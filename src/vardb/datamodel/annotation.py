"""varDB datamodel Annotation class"""
from datetime import datetime
from typing import TYPE_CHECKING, Optional

import pytz
from sqlalchemy import DateTime, FetchedValue, ForeignKey, Index, Integer
from sqlalchemy.dialects.postgresql import JSON, JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict, JSONMutableList

if TYPE_CHECKING:
    from vardb.datamodel.allele import Allele
    from vardb.datamodel.user import User


class Annotation(Base):
    """Represents a set of annotations for an allele"""

    __tablename__ = "annotation"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    allele_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("allele.id"), index=True)
    allele: Mapped[Optional["Allele"]] = relationship("Allele", uselist=False)
    annotations: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB))
    schema_version: Mapped[int] = mapped_column(
        Integer, nullable=False, server_default=FetchedValue()
    )
    previous_annotation_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("annotation.id"))
    # use remote_side to store foreignkey for previous_annotation in 'this' parent:
    previous_annotation: Mapped["Annotation"] = relationship(
        "Annotation", uselist=False, remote_side=id
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(
        "date_superceeded", DateTime(timezone=True)
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    annotation_config_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("annotationconfig.id"), index=True, nullable=False
    )
    annotation_config: Mapped["AnnotationConfig"] = relationship(
        "AnnotationConfig", backref="annotations"
    )

    def __repr__(self):
        return "<Annotation('%s', '%s', '%s')>" % (
            self.annotations,
            self.previous_annotation,
            self.date_superceeded,
        )


Index(
    "ix_annotation_unique",
    Annotation.allele_id,
    postgresql_where=(Annotation.date_superceeded.is_(None)),
    unique=True,
)


class CustomAnnotation(Base):
    """Represents a set of annotations for an allele, created by a user"""

    __tablename__ = "customannotation"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    annotations: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB))

    allele_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("allele.id"))
    allele: Mapped[Optional["Allele"]] = relationship("Allele", uselist=False)
    user_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("user.id"))
    user: Mapped[Optional["User"]] = relationship("User", uselist=False)
    previous_annotation_id: Mapped[int | None] = mapped_column(
        "previous_annotation_id", Integer, ForeignKey("customannotation.id")
    )
    # use remote_side to store foreignkey for previous_annotation in 'this' parent:
    previous_annotation: Mapped[Optional["CustomAnnotation"]] = relationship(
        "CustomAnnotation", uselist=False, remote_side=id
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )

    def __repr__(self):
        return "<CustomAnnotation('%s')>" % (self.annotations)


Index(
    "ix_customannotation_unique",
    CustomAnnotation.allele_id,
    postgresql_where=(CustomAnnotation.date_superceeded.is_(None)),
    unique=True,
)


class AnnotationConfig(Base):
    __tablename__ = "annotationconfig"
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    # Use JSON instead of JSONB, to preserve order of keys
    deposit: Mapped[dict] = mapped_column(
        JSONMutableList.as_mutable(JSON), nullable=False, default=lambda: {}  # type: ignore[arg-type]
    )
    view: Mapped[dict] = mapped_column(
        JSONMutableList.as_mutable(JSON), nullable=False, default=lambda: {}  # type: ignore[arg-type]
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
