import datetime

import pytz
from sqlalchemy import BigInteger, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import mapped_column, relationship

from vardb.datamodel import Base


class Attachment(Base):
    __tablename__ = "attachment"

    id = mapped_column(Integer, primary_key=True)
    sha256 = mapped_column(String())
    filename = mapped_column(String(), nullable=False)
    size = mapped_column(BigInteger)
    date_created = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.datetime.now(pytz.utc)
    )
    mimetype = mapped_column(String())
    extension = mapped_column(String())
    user_id = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    user = relationship("User", uselist=False)
