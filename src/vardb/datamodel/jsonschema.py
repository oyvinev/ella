from sqlalchemy import Index, Integer
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict


class JSONSchema(Base):
    """Table for all JSON schemas used in the application"""

    __tablename__ = "jsonschema"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str]
    version: Mapped[int]
    schema: Mapped[dict] = mapped_column(JSONMutableDict.as_mutable(JSONB), nullable=False)

    def __repr__(self):
        return "<JSONSchema('%s', '%s')>" % (self.name, self.version)


Index("ix_jsonschema_unique", JSONSchema.name, JSONSchema.version, unique=True)
