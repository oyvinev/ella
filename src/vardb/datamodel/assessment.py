"""vardb datamodel Assessment class"""
from datetime import datetime
from typing import TYPE_CHECKING, Optional

import pytz
from sqlalchemy import Boolean, Column, DateTime, Enum, ForeignKey, Integer, String, Table
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, Index
from sqlalchemy_searchable import SearchQueryMixin
from sqlalchemy_utils.types import TSVectorType

from vardb.datamodel import Base
from vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from vardb.datamodel.allele import Allele
    from vardb.datamodel.annotation import Annotation, CustomAnnotation
    from vardb.datamodel.attachment import Attachment
    from vardb.datamodel.gene import Gene, Genepanel
    from vardb.datamodel.user import User, UserGroup

AlleleAssessmentReferenceAssessment = Table(
    "alleleassessmentreferenceassessment",
    Base.metadata,
    Column("alleleassessment_id", Integer, ForeignKey("alleleassessment.id")),
    Column("referenceassessment_id", Integer, ForeignKey("referenceassessment.id")),
)

AlleleAssessmentAttachment = Table(
    "alleleassessmentattachment",
    Base.metadata,
    Column("alleleassessment_id", Integer, ForeignKey("alleleassessment.id")),
    Column("attachment_id", Integer, ForeignKey("attachment.id")),
)


class AlleleAssessment(Base):
    """Represents an assessment of one allele."""

    __tablename__ = "alleleassessment"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    classification: Mapped[str] = mapped_column(
        Enum("1", "2", "3", "4", "5", "NP", "RF", "DR", name="alleleassessment_classification"),
        nullable=False,
    )
    evaluation: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    user: Mapped["User"] = relationship("User", uselist=False)
    usergroup_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("usergroup.id"))
    usergroup: Mapped[Optional["UserGroup"]] = relationship("UserGroup", uselist=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_assessment_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("alleleassessment.id")
    )
    previous_assessment: Mapped[Optional["AlleleAssessment"]] = relationship(
        "AlleleAssessment", uselist=False
    )
    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    allele: Mapped["Allele"] = relationship("Allele", uselist=False, backref="assessments")
    genepanel_name: Mapped[str] = mapped_column(String, nullable=False)
    genepanel_version: Mapped[str] = mapped_column(String, nullable=False)
    genepanel: Mapped["Genepanel"] = relationship("Genepanel", uselist=False)
    analysis_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="SET NULL")
    )
    annotation_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("annotation.id"))
    annotation: Mapped[Optional["Annotation"]] = relationship("Annotation")
    custom_annotation_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("customannotation.id")
    )
    custom_annotation: Mapped[Optional["CustomAnnotation"]] = relationship("CustomAnnotation")

    referenceassessments: Mapped[list["ReferenceAssessment"]] = relationship(
        "ReferenceAssessment", secondary=AlleleAssessmentReferenceAssessment
    )
    attachments: Mapped[list["Attachment"]] = relationship(
        "Attachment", secondary=AlleleAssessmentAttachment
    )

    __table_args__ = (
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    def __repr__(self):
        return "<AlleleAssessment('%s','%s', '%s')>" % (
            self.id,
            self.allele_id,
            self.classification,
        )

    def __str__(self):
        return "%s, %s" % (self.classification, self.date_created)


Index(
    "ix_assessment_alleleid_unique",
    AlleleAssessment.allele_id,
    postgresql_where=(AlleleAssessment.date_superceeded.is_(None)),
    unique=True,
)


class ReferenceAssessment(Base):
    """Association object between assessments and references.

    Note that Assessment uses an association proxy;
    usage of AssessmentReference can therefore be sidestepped
    if it is not necessary to change values to the extra attributes in this class.
    """

    __tablename__ = "referenceassessment"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    reference_id: Mapped[int] = mapped_column(Integer, ForeignKey("reference.id"), nullable=False)
    reference: Mapped["Reference"] = relationship("Reference")
    evaluation: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    user: Mapped["User"] = relationship("User", uselist=False)
    usergroup_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("usergroup.id"))
    usergroup: Mapped[Optional["UserGroup"]] = relationship("UserGroup", uselist=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    genepanel_name: Mapped[str] = mapped_column(String, nullable=False)
    genepanel_version: Mapped[str] = mapped_column(String, nullable=False)
    genepanel: Mapped["Genepanel"] = relationship("Genepanel", uselist=False)
    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    allele: Mapped["Allele"] = relationship("Allele", uselist=False)
    previous_assessment_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("referenceassessment.id")
    )
    previous_assessment: Mapped[Optional["ReferenceAssessment"]] = relationship(
        "ReferenceAssessment", uselist=False
    )
    analysis_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="SET NULL")
    )
    __table_args__ = (
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    def __str__(self):
        return "%s, %s, %s" % (str(self.user), self.reference, self.evaluation)


Index(
    "ix_referenceassessment_alleleid_referenceid_unique",
    ReferenceAssessment.allele_id,
    ReferenceAssessment.reference_id,
    postgresql_where=(ReferenceAssessment.date_superceeded.is_(None)),
    unique=True,
)


class Reference(Base, SearchQueryMixin):
    """Represents a reference that brings information to this assessment."""

    __tablename__ = "reference"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    authors: Mapped[str | None]
    title: Mapped[str | None]
    journal: Mapped[str | None]
    abstract: Mapped[str | None]
    year: Mapped[str | None]
    pubmed_id: Mapped[int | None] = mapped_column(Integer, unique=True)
    published: Mapped[bool] = mapped_column(Boolean(), default=True, nullable=False)
    attachment_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("attachment.id"))
    attachment: Mapped[Optional["Attachment"]] = relationship("Attachment", uselist=False)

    search = mapped_column(
        TSVectorType(
            "authors",
            "title",
            "journal",
            "year",
            weights={"authors": "A", "title": "A", "journal": "B", "year": "C"},
        )
    )

    __table_args__ = (Index("ix_pubmedid", "pubmed_id", unique=True),)

    def __repr__(self):
        return "<Reference('%s','%s', '%s')>" % (self.authors, self.title, self.year)

    def __str__(self):
        return "%s (%s) %s" % (self.authors, self.year, self.journal)

    def citation(self):
        return "%s (%s) %s %s" % (self.authors, self.year, self.title, self.journal)


class AlleleReport(Base):
    """Represents a report for one allele. The report is aimed at the
    clinicians as compared to alleleassessment which is aimed at fellow
    interpreters. The report might not change as often as the alleleassessment."""

    __tablename__ = "allelereport"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    evaluation: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    user: Mapped["User"] = relationship("User", uselist=False)
    usergroup_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("usergroup.id"))
    usergroup: Mapped[Optional["UserGroup"]] = relationship("UserGroup", uselist=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_report_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("allelereport.id"))
    previous_report: Mapped[Optional["AlleleReport"]] = relationship("AlleleReport", uselist=False)
    allele_id: Mapped[int] = mapped_column(Integer, ForeignKey("allele.id"), nullable=False)
    allele: Mapped["Allele"] = relationship("Allele", uselist=False, backref="reports")
    analysis_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="SET NULL")
    )
    alleleassessment_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("alleleassessment.id")
    )
    alleleassessment: Mapped["AlleleAssessment"] = relationship("AlleleAssessment")

    def __repr__(self):
        return "<AlleleReport('%s','%s', '%s')>" % (self.id, self.allele_id, str(self.user))


Index(
    "ix_allelereport_alleleid_unique",
    AlleleReport.allele_id,
    postgresql_where=(AlleleReport.date_superceeded.is_(None)),
    unique=True,
)


class GeneAssessment(Base):
    """Represents an assessment for a single gene."""

    __tablename__ = "geneassessment"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    evaluation: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey("user.id"), nullable=False)
    user: Mapped["User"] = relationship("User", uselist=False)
    usergroup_id: Mapped[int | None] = mapped_column(Integer, ForeignKey("usergroup.id"))
    usergroup: Mapped[Optional["UserGroup"]] = relationship("UserGroup", uselist=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=False, default=lambda: datetime.now(pytz.utc)
    )
    genepanel_name: Mapped[str] = mapped_column(String, nullable=False)
    genepanel_version: Mapped[str] = mapped_column(String, nullable=False)
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_assessment_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("geneassessment.id")
    )
    previous_assessment: Mapped[Optional["GeneAssessment"]] = relationship(
        "GeneAssessment", uselist=False
    )
    gene_id: Mapped[int] = mapped_column(Integer, ForeignKey("gene.hgnc_id"), nullable=False)
    gene: Mapped["Gene"] = relationship("Gene", uselist=False, backref="assessments")
    analysis_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("analysis.id", ondelete="SET NULL")
    )

    def __repr__(self):
        return "<GeneAssessment('%s','%s', '%s')>" % (self.id, self.gene_id, str(self.user))


Index(
    "ix_geneassessment_geneid_unique",
    GeneAssessment.gene_id,
    postgresql_where=(GeneAssessment.date_superceeded.is_(None)),
    unique=True,
)
