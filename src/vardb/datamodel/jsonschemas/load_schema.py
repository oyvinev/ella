import json
from pathlib import Path

import jsonref

SCRIPT_PATH = Path(__file__).parent.absolute()


def load_schema(schemaname: str):
    with (SCRIPT_PATH / schemaname).open("r") as f:
        return jsonref.replace_refs(
            json.load(f),
            jsonschema=True,
            base_uri="file:{}/".format(SCRIPT_PATH),
            load_on_repr=True,
        )
