from sqlalchemy import select

from datalayer import filters, queries
from datalayer.allelefilter.filterbase import FilterBase
from vardb.datamodel import assessment


class ClassificationFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: dict[str, list[str]]
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids,
        that have an existing classification in the provided filter_config['classes'],
        and are not outdated per the application config, if flag `exclude_outdated` is set to True.

        `exclude_outdated` defaults to False, to keep backwards compatibility. This is suitable when used as an exception.
        Setting this to True is more suitable when sued as a forward filter, to e.g. filter out class 2 variants only if
        they have a valid date.
        """
        filter_classes = filter_config["classes"]
        available_classes = list(
            assessment.AlleleAssessment.classification.property.columns[0].type.enums
        )

        assert not set(filter_classes) - set(
            available_classes
        ), "Invalid class(es) to filter on in {}. Available classes are {}.".format(
            filter_classes, available_classes
        )

        # Only apply filter to assessments within date set in config if exclude_outdated is True.
        # Note: date_superceeded is _not_ related "outdatedness", this is to just get the latest
        # assessments for the allele (regardless of date)
        if filter_config.get("exclude_outdated", False):
            valid_assessments = queries.valid_alleleassessments_filter(self.session)
        else:
            valid_assessments = [assessment.AlleleAssessment.date_superceeded.is_(None)]

        filtered_allele_ids = (
            self.session.execute(
                select(assessment.AlleleAssessment.allele_id).filter(
                    filters.in_(self.session, assessment.AlleleAssessment.allele_id, allele_ids),
                    assessment.AlleleAssessment.classification.in_(filter_classes),
                    *valid_assessments
                )
            )
            .scalars()
            .all()
        )

        return set(filtered_allele_ids)
