from typing import Any

from sqlalchemy import CompoundSelect, Select, and_, func, not_, or_, select, text, union

from datalayer.allelefilter.filterbase import FilterBase
from datalayer.genotypetable import get_genotype_temp_table
from vardb.datamodel import annotationshadow, gene, sample
from vardb.util.extended_query import temp_table

FILTER_MODES = ["recessive_non_candidates", "recessive_candidates"]


class InheritanceModelFilter(FilterBase):
    CONTEXT_DEPENDENT = True
    FILTER_WITH = "analysis"

    def filter_with_analysis(
        self, analysis_id: int, allele_ids: list[int], filter_config: dict[str, Any]
    ) -> set[int]:
        """
        Works on proband data only, does *not* use family information.
        Intended only for usage on single sample analyses.

        Filter removing variants not consistent with inheritence model.

        Filter criterias for the different modes:
            recessive_non_candidates:
                - single, heterozygous variant
                - distinct AR inheritance
            recessive_candidates:
                - single homozygous variant or multiple variants
                - not distinct AD inheritance

        "recessive_candidates" is intended to be used when filter is
        run as an exceptions filter, to rescue potentially important
        variants from being filtered by another filter.

        Config example:
        {
            "filter_mode": "recessive_non_candidates"
        }


        """

        if "filter_mode" not in filter_config:
            raise RuntimeError("Filter configuration is missing required config key 'filter_mode'")

        filter_mode = filter_config["filter_mode"]
        assert (
            filter_mode in FILTER_MODES
        ), f"Configuration key filter_mode must be one of: {', '.join(FILTER_MODES)}"

        gp_name, gp_version = self.session.execute(
            select(sample.Analysis.genepanel_name, sample.Analysis.genepanel_version).filter(
                sample.Analysis.id == analysis_id
            )
        ).one()

        # There can be multiple sample ids for a proband,
        # we need to check them all
        proband_sample_ids = (
            self.session.execute(
                select(sample.Sample.id).filter(
                    sample.Sample.analysis_id == analysis_id, sample.Sample.proband.is_(True)
                )
            )
            .scalars()
            .all()
        )

        # We need to merge all the proband samples together into one table
        # Table we get from genotype_table looks like the following:
        # -------------------------------------------------------------------------------------
        # | allele_id | Sample 1_id | Sample 1_type | ... | Sample 2_id | Sample 2_type | ... |
        # -------------------------------------------------------------------------------------
        # | 62        | 69          | Heterozygous  | ... | None        | None          | ... |
        # | 63        | 70          | Homozygous    | ... | None        | None          | ... |
        # | 64        | None        | None          | ... | 71          | Heterozygous  | ... |
        #
        # Which we transform into the following:
        # -------------------------------
        # | allele_id | proband_genotype |
        # -------------------------------
        # | 62        | Heterozygous     |
        # | 63        | Homozygous       |
        # | 64        | Heterozygous     |

        genotype_table = get_genotype_temp_table(self.session, allele_ids, proband_sample_ids)

        proband_genotype_tables: list[Select] = list()
        for proband_sample_id in proband_sample_ids:
            proband_genotype_tables.append(
                select(
                    genotype_table.c.allele_id.label("allele_id"),
                    getattr(genotype_table.c, f"{proband_sample_id}_type").label(
                        "proband_genotype"
                    ),
                ).filter(~getattr(genotype_table.c, f"{proband_sample_id}_type").is_(None))
            )

        proband_genotype_table: Select | CompoundSelect

        # = proband_genotype_tables[0]
        if len(proband_genotype_tables) > 1:
            proband_genotype_table = union(*proband_genotype_tables)
        else:
            proband_genotype_table = proband_genotype_tables[0]

        proband_genotype_table = temp_table(
            self.session, proband_genotype_table, "proband_genotype_table"
        )

        #
        # While the criterias themselves are pretty straightforward,
        # there are some cases making it more complicated
        #
        # Simple case:
        # allele_id  hgnc_id  inheritance
        # 1          1001     AR
        # 2          1001     AR
        #
        # One variant has two genes:
        # Here we cannot simple group by hgnc_id and check,
        # we need to check across genes.
        # allele_id  hgnc_id  inheritance
        # 1          1001     AR
        # 2          1001     AR
        # 2          1002     AD
        #
        # Same as above, but different inheritance, making it more dangerous:
        # Only grouping by hgnc_id would filter wrongly,
        # since allele 2 would be filtered for mode recessive_non_candidates.
        # allele_id  hgnc_id  inheritance
        # 1          1001     AR
        # 2          1001     AR
        # 2          1002     AR
        #
        # And another variation where both has multiple genes:
        # We need to make sure the criterias are fulfilled for all genes
        # allele_id  hgnc_id  inheritance
        # 1          1001     AR
        # 1          1002     AR
        # 2          1001     AR
        # 2          1002     AR
        #

        # Get genepanel transcript' inheritance per hgnc id
        # ------------------------
        # | hgnc_id | inheritance |
        # ------------------------
        # | 7       | AD          |
        # | 7       | AR          |
        # | 13666   | AR          |
        # ...
        genepanel_hgnc_id_inheritance = (
            select(
                gene.Transcript.gene_id.label("hgnc_id"),
                gene.genepanel_transcript.c.inheritance,
            )
            .filter(
                gene.Transcript.id == gene.genepanel_transcript.c.transcript_id,
                gene.genepanel_transcript.c.genepanel_name == gp_name,
                gene.genepanel_transcript.c.genepanel_version == gp_version,
            )
            .subquery("genepanel_hgnc_id_inheritance")
        )

        # Set up column criterias from filter_config
        criteria_columns = []
        if filter_mode == "recessive_non_candidates":
            # - single, heterozygous variant
            # - distinct AR
            criteria_columns = [
                func.bool_and(genepanel_hgnc_id_inheritance.c.inheritance == "AR").label(
                    "is_inheritance_match"
                ),
                and_(
                    func.count(proband_genotype_table.c.allele_id.distinct()) == 1,
                    func.bool_and(proband_genotype_table.c.proband_genotype == "Heterozygous"),
                ).label("is_variant_match"),
            ]
        elif filter_mode == "recessive_candidates":
            # - single homozygous variant or multiple variants
            # - not distinct AD inheritance
            criteria_columns = [
                not_(func.bool_and(genepanel_hgnc_id_inheritance.c.inheritance == "AD")).label(
                    "is_inheritance_match"
                ),
                or_(
                    # single variant, at least one sample has homozygous genotype
                    and_(
                        func.count(proband_genotype_table.c.allele_id.distinct()) == 1,
                        func.bool_or(proband_genotype_table.c.proband_genotype == "Homozygous"),
                    ),
                    # more than one variant (in which case genotype is irrelevant)
                    func.count(proband_genotype_table.c.allele_id.distinct()) > 1,
                ).label("is_variant_match"),
            ]
        else:
            raise RuntimeError("Wrong filter_mode")

        # Set up filters
        filters = []
        if self.config and "inclusion_regex" in self.config["transcripts"]:
            filters.append(
                text("transcript ~ :reg").params(reg=self.config["transcripts"]["inclusion_regex"])
            )

        # Join all data, group by hgnc_id and aggregate data to
        # generate each critera *per hgnc_id*
        # | allele_id | hgnc_id | is_inheritance_match | is_variant_match |
        # -----------------------------------------------------------------
        # | 1152      | 7       | False                | True             |
        # | 1521      | 20      | False                | True             |
        # | 1474      | 23      | True                 | False            |
        # | 1475      | 23      | True                 | False            |
        allele_ids_filter_candidates = (
            select(
                func.unnest(func.array_agg(proband_genotype_table.c.allele_id)).label("allele_id"),
                annotationshadow.AnnotationShadowTranscript.hgnc_id,
                *criteria_columns,
            )
            .join(
                annotationshadow.AnnotationShadowTranscript,
                proband_genotype_table.c.allele_id
                == annotationshadow.AnnotationShadowTranscript.allele_id,
            )
            .join(
                genepanel_hgnc_id_inheritance,
                genepanel_hgnc_id_inheritance.c.hgnc_id
                == annotationshadow.AnnotationShadowTranscript.hgnc_id,
            )
            .filter(*filters)
            .group_by(annotationshadow.AnnotationShadowTranscript.hgnc_id)
            .order_by(annotationshadow.AnnotationShadowTranscript.hgnc_id)
            .distinct()
            .subquery()
        )

        # Finally group by allele ids, and check criterias
        # across all the genes in the genepanel (per allele id).
        # We need to do this since alleles can overlap multiple genes.

        # If recessive_non_candidates, the criterias need to be true for all genes
        # If recessive_candidates, the criterias need to be true for one or more genes

        agg_func = None
        if filter_mode == "recessive_non_candidates":
            agg_func = func.bool_and
        elif filter_mode == "recessive_candidates":
            agg_func = func.bool_or
        else:
            raise RuntimeError("Wrong filter_mode")

        having_criteria = agg_func(
            and_(
                allele_ids_filter_candidates.c.is_inheritance_match.is_(True),
                allele_ids_filter_candidates.c.is_variant_match.is_(True),
            )
        )
        filtered_allele_ids: Select[tuple[int]] = (
            select(allele_ids_filter_candidates.c.allele_id)
            .group_by(allele_ids_filter_candidates.c.allele_id)
            .having(having_criteria)
        )

        result = set(self.session.execute(filtered_allele_ids).scalars().all())
        return result
