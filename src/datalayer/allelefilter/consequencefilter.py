from typing import Any

from sqlalchemy import or_, select, text, tuple_
from sqlalchemy.orm.attributes import InstrumentedAttribute

from datalayer import filters
from datalayer.allelefilter.filterbase import FilterBase
from vardb.datamodel import annotationshadow, gene


class ConsequenceFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "genepanel"

    def filter_with_genepanel(
        self,
        gp_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: dict[str, Any],
    ) -> set[int]:
        """
        Filter alleles that have consequence in list of consequences for any of the transcripts matching
        the global include regex (if specified). Can be specified to look at genepanel genes only.
        """
        assert self.config
        gp_csq_only = filter_config.get("genepanel_only", False)

        consequences = filter_config["consequences"]
        assert not set(consequences) - set(
            self.config["transcripts"]["consequences"]
        ), "Invalid consequences passed to filter: {}".format(consequences)

        ast_allele_id: InstrumentedAttribute[
            int
        ] = annotationshadow.AnnotationShadowTranscript.allele_id
        allele_ids_with_consequence = (
            select(ast_allele_id)
            .filter(
                filters.in_(
                    self.session, annotationshadow.AnnotationShadowTranscript.allele_id, allele_ids
                ),
                annotationshadow.AnnotationShadowTranscript.consequences.op("&&")(consequences),
            )
            .distinct()
        )

        inclusion_regex = self.config.get("transcripts", {}).get("inclusion_regex")
        if inclusion_regex:
            allele_ids_with_consequence = allele_ids_with_consequence.filter(
                text("transcript ~ :reg").params(reg=inclusion_regex)
            )

        # Only include genes in genepanel if flag gp_csq_only
        if gp_csq_only:
            gp_genes = self.session.execute(
                select(gene.Transcript.gene_id, gene.Gene.hgnc_symbol)
                .join(gene.Genepanel.transcripts)
                .join(gene.Gene)
                .filter(tuple_(gene.Genepanel.name, gene.Genepanel.version) == gp_key)
            )

            gp_gene_ids, gp_gene_symbols = list(zip(*[(g[0], g[1]) for g in gp_genes]))

            allele_ids_with_consequence = allele_ids_with_consequence.filter(
                or_(
                    filters.in_(
                        self.session,
                        annotationshadow.AnnotationShadowTranscript.hgnc_id,
                        gp_gene_ids,
                    ),
                    filters.in_(
                        self.session,
                        annotationshadow.AnnotationShadowTranscript.symbol,
                        gp_gene_symbols,
                    ),
                )
            )
        return set(self.session.execute(allele_ids_with_consequence).scalars().all())
