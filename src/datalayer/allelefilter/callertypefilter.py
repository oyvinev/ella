from sqlalchemy import select

from datalayer.allelefilter.filterbase import FilterBase
from vardb.datamodel import allele


class CallerTypeFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: dict[str, list[str]]
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids,
        that have an existing callerType in the provided filter_config['callertype'].
        """

        available_callertypes = list(allele.Allele.caller_type.property.columns[0].type.enums)
        filter_callertypes = filter_config["callerTypes"]

        assert filter_callertypes, "callerTypes was not found in filterconfig"

        assert not set(filter_callertypes) - set(
            available_callertypes
        ), "Invalid callertype(s) to filter on in {}. Available callertypes are {}.".format(
            filter_callertypes, available_callertypes
        )

        filtered_allele_ids = (
            self.session.execute(
                select(allele.Allele.id).filter(
                    allele.Allele.id.in_(allele_ids),
                    allele.Allele.caller_type.in_(filter_callertypes),
                )
            )
            .scalars()
            .all()
        )
        return set(filtered_allele_ids)
