from typing import Any

from sqlalchemy import and_, not_, or_, select

from datalayer.allelefilter.filterbase import FilterBase
from datalayer.genotypetable import get_genotype_temp_table
from vardb.datamodel import sample


class QualityFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "analysis"

    def filter_with_analysis(
        self, analysis_id: int, allele_ids: list[int], filter_config: dict[str, Any]
    ) -> set[int]:
        """
        Returns allele_ids that can be filtered _out_ from an analysis.
        """
        assert any(k in filter_config for k in ["qual", "allele_ratio", "filter_status"])

        proband_sample_ids = (
            self.session.execute(
                select(sample.Sample.id).filter(
                    sample.Sample.analysis_id == analysis_id, sample.Sample.proband.is_(True)
                )
            )
            .scalars()
            .all()
        )

        genotype_table = get_genotype_temp_table(
            self.session,
            allele_ids,
            proband_sample_ids,
            genotype_extras={"qual": "variant_quality", "filter_status": "filter_status"},
            genotypesampledata_extras={"ar": "allele_ratio"},
        )

        # We consider an allele to be filtered out if it is to be filtered out in ALL proband samples it occurs in.
        # To achieve this, we work backwards, by finding all alleles that should not be filtered in each sample, and
        # subtracting it from the set of all allele ids.
        considered_allele_ids: set[int] = set()
        filtered_allele_ids = set(allele_ids)

        for sample_id in proband_sample_ids:
            filter_conditions = []
            if "qual" in filter_config:
                filter_conditions.extend(
                    [
                        ~getattr(genotype_table.c, f"{sample_id}_qual").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_qual") < filter_config["qual"],
                    ]
                )

            if "allele_ratio" in filter_config:
                filter_conditions.extend(
                    [
                        ~getattr(genotype_table.c, f"{sample_id}_ar").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_ar")
                        < filter_config["allele_ratio"],
                        getattr(genotype_table.c, f"{sample_id}_ar")
                        != 0.0,  # Allele ratios can sometimes be misleading 0.0. Avoid filtering these out.,
                    ]
                )

            if "filter_status" in filter_config:
                # Filter according to regex pattern
                filter_status_pattern = getattr(genotype_table.c, f"{sample_id}_filter_status").op(
                    "~"
                )(filter_config["filter_status"]["pattern"])

                # If filter_empty, always filter out None or '.'
                if filter_config["filter_status"].get("filter_empty", False):
                    filter_status_condition = or_(
                        filter_status_pattern,
                        getattr(genotype_table.c, f"{sample_id}_filter_status").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_filter_status") == ".",
                    )
                # else, never filter out None or '.' (even if regex says '\.')
                else:
                    filter_status_condition = and_(
                        filter_status_pattern,
                        ~getattr(genotype_table.c, f"{sample_id}_filter_status").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_filter_status") != ".",
                    )

                # If inverse, run whole expression in reverse
                if filter_config["filter_status"].get("inverse", False):
                    filter_status_condition = not_(filter_status_condition)

                filter_conditions.append(filter_status_condition)

            alleles_in_sample = select(genotype_table.c.allele_id).filter(
                ~getattr(genotype_table.c, f"{sample_id}_genotypeid").is_(None)
            )
            considered_allele_ids |= set(self.session.execute(alleles_in_sample).scalars().all())

            not_filtered_allele_ids = alleles_in_sample.filter(~and_(*filter_conditions))
            filtered_allele_ids -= set(
                self.session.execute(not_filtered_allele_ids).scalars().all()
            )

        assert considered_allele_ids == set(allele_ids)
        return filtered_allele_ids
