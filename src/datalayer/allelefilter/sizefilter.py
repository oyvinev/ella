import operator
from typing import Any

from sqlalchemy import select

from datalayer.allelefilter.filterbase import FilterBase
from vardb.datamodel import allele

OPERATORS = {
    "==": operator.eq,
    ">=": operator.ge,
    "<=": operator.le,
    ">": operator.gt,
    "<": operator.lt,
}


class SizeFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    def filter_with_alleles(self, allele_ids: list[int], filter_config: dict[str, Any]) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids, that have an allele
        length specified by the size in threshold, and operator set in mode.

        Example configuration:

        {
            "name": "size",
            "config": {
                "threshold": 10000,
                "mode": ">"
            }
        }

        Keeping all alleles with length above 10kb
        """

        return set(
            self.session.execute(
                select(allele.Allele.id).filter(
                    allele.Allele.id.in_(allele_ids),
                    OPERATORS[filter_config["mode"]](
                        allele.Allele.length, filter_config["threshold"]
                    ),
                )
            )
            .scalars()
            .all()
        )
