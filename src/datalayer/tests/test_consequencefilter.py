"""
Integration/unit test for the AlleleFilter module.
Since it consists mostly of database queries, it's tested on a live database.
"""
from dataclasses import asdict, dataclass
from typing import Literal

import hypothesis as ht
import hypothesis.strategies as st
import pytest
from sqlalchemy import text
from sqlalchemy.orm.session import Session

from conftest import mock_allele_with_annotation
from datalayer.allelefilter.consequencefilter import ConsequenceFilter
from vardb.datamodel import annotationshadow, gene
from vardb.util.testdatabase import TestDatabase

# prevent screen getting filled with output (useful when testing manually)
# import logging
# logging.getLogger('vardb.deposit.deposit_genepanel').setLevel(logging.CRITICAL)


GLOBAL_CONFIG = {
    "frequencies": {
        "groups": {
            "external": {"ExAC": ["G", "FIN"], "1000g": ["G"], "esp6500": ["AA", "EA"]},
            "internal": {"inDB": ["AF"]},
        }
    },
    "transcripts": {
        "consequences": [
            "transcript_ablation",
            "splice_acceptor_variant",
            "splice_donor_variant",
            "stop_gained",
            "frameshift_variant",
            "stop_lost",
            "start_lost",
            "initiator_codon_variant",
            "transcript_amplification",
            "inframe_insertion",
            "inframe_deletion",
            "missense_variant",
            "protein_altering_variant",
            "splice_region_variant",
            "splice_donor_5th_base_variant",
            "splice_donor_region_variant",
            "splice_polypyrimidine_tract_variant",
            "incomplete_terminal_codon_variant",
            "start_retained_variant",
            "stop_retained_variant",
            "synonymous_variant",
            "coding_sequence_variant",
            "mature_miRNA_variant",
            "5_prime_UTR_variant",
            "3_prime_UTR_variant",
            "non_coding_transcript_exon_variant",
            "intron_variant",
            "NMD_transcript_variant",
            "non_coding_transcript_variant",
            "upstream_gene_variant",
            "downstream_gene_variant",
            "TFBS_ablation",
            "TFBS_amplification",
            "TF_binding_site_variant",
            "regulatory_region_ablation",
            "regulatory_region_amplification",
            "feature_elongation",
            "regulatory_region_variant",
            "feature_truncation",
            "intergenic_variant",
        ],
        "inclusion_regex": "NM_.*",
    },
}


@st.composite
def allele_positions(draw: st.DrawFn, chromosome: str, start: int, end: int):
    start_position = draw(st.integers(min_value=start, max_value=end))
    end_position = draw(st.integers(min_value=start_position + 1, max_value=start_position + 50))
    return (chromosome, start_position, end_position)


allele_start = 1300


def create_genepanel(session: Session):
    # Create fake genepanel for testing purposes

    g1 = gene.Gene(hgnc_id=int(1e6), hgnc_symbol="GENE1")

    t1 = gene.Transcript(
        gene=g1,
        transcript_name="NM_1",
        type="RefSeq",
        genome_reference="",
        tags=None,
        chromosome="1",
        tx_start=1000,
        tx_end=1500,
        strand="+",
        cds_start=1230,
        cds_end=1430,
        exon_starts=[1100, 1200, 1300, 1400],
        exon_ends=[1160, 1260, 1360, 1460],
    )
    session.add(t1)

    genepanel = gene.Genepanel(name="testpanel", version="v01", genome_reference="GRCh37")
    session.add(genepanel)
    session.flush()
    session.execute(
        gene.genepanel_transcript.insert(),
        {
            "transcript_id": t1.id,
            "genepanel_name": genepanel.name,
            "genepanel_version": genepanel.version,
            "inheritance": "AD/AR",
        },
    )

    return genepanel


@dataclass
class FC:
    consequences: list[str]
    genepanel_only: bool


@st.composite
def filter_config(draw: st.DrawFn):
    consequences = draw(
        st.lists(elements=st.sampled_from(GLOBAL_CONFIG["transcripts"]["consequences"]), min_size=1)  # type: ignore[index]
    )
    genepanel_only = draw(st.booleans())
    return FC(consequences, genepanel_only)


@dataclass
class TX:
    symbol: Literal["GENE1", "SOME_RANDOM_GENE"]
    transcript: Literal["NM_1.1", "NM_SOMETHING", "SOME_OTHER_TRANSCRIPT"]
    consequences: list[str]


@st.composite
def transcripts(draw: st.DrawFn):
    N = draw(st.integers(min_value=1, max_value=10))
    tx: list[list[TX]] = []
    for i in range(N):
        gene_symbol = draw(st.sampled_from(("GENE1", "SOME_RANDOM_GENE")))
        transcript = draw(st.sampled_from(("NM_1.1", "NM_SOMETHING", "SOME_OTHER_TRANSCRIPT")))
        consequences = draw(
            st.lists(
                elements=st.sampled_from(GLOBAL_CONFIG["transcripts"]["consequences"]), min_size=1  # type: ignore[index]
            )
        )
        tx.append(TX(gene_symbol, transcript, consequences))  # type: ignore[arg-type]

    return tx


class TestConsequenceFilter(object):
    @pytest.mark.aa(order=0)
    def test_prepare_data(self, test_database: TestDatabase, session: Session):
        test_database.refresh()  # Reset db

        # We need to recreate the annotation shadow tables,
        # since we want to use our test config
        # Delete existing filterconfigs and usergroups to avoid errors
        # when creating new shadow tables
        session.execute(text("DELETE FROM usergroupfilterconfig"))
        session.execute(text("DELETE FROM filterconfig"))
        session.execute(text("UPDATE usergroup SET config='{}'"))
        annotationshadow.create_shadow_tables(session, GLOBAL_CONFIG)

        create_genepanel(session)
        session.commit()

    @ht.given(st.one_of(filter_config()), st.lists(transcripts(), min_size=1))
    def test_consequence_filter(
        self, session: Session, filter_config: FC, transcripts: list[list[TX]]
    ):
        session.rollback()
        genepanel_consequences = dict()
        all_consequences = dict()
        allele_ids = []
        for tx in transcripts:
            al, _ = mock_allele_with_annotation(
                session, annotations={"transcripts": [asdict(_tx) for _tx in tx]}
            )
            session.flush()
            allele_ids.append(al.id)
            include_tx = [t for t in tx if t.transcript.startswith("NM_")]
            genepanel_consequences[al.id] = set(
                sum([t.consequences for t in include_tx if t.symbol == "GENE1"], [])
            )
            all_consequences[al.id] = set(sum([t.consequences for t in include_tx], []))

        cf = ConsequenceFilter(session, GLOBAL_CONFIG)
        result = cf.filter_with_genepanel(("testpanel", "v01"), allele_ids, asdict(filter_config))

        expected_result = set()
        check_consequences = (
            genepanel_consequences if filter_config.genepanel_only else all_consequences
        )
        for a_id in allele_ids:
            if set(filter_config.consequences) & check_consequences[a_id]:
                expected_result.add(a_id)

        assert result == expected_result
