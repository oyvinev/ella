from typing import Any, Iterable, Literal, Tuple, Union, overload

from sqlalchemy import Column
from sqlalchemy import Tuple as SQLTuple
from sqlalchemy import cast, func, literal_column, select, tuple_
from sqlalchemy.dialects.postgresql import ARRAY, array
from sqlalchemy.engine.row import Row

# from sqlalchemy.orm.attributes import ColumnOperators
from sqlalchemy.orm import InstrumentedAttribute, Query, Session
from sqlalchemy.sql.expression import false
from sqlalchemy.sql.operators import ColumnOperators
from sqlalchemy.sql.selectable import Selectable
from sqlalchemy.types import Integer, String

AttrIntStr = InstrumentedAttribute[int] | InstrumentedAttribute[str]


@overload
def in_(
    session: Session,
    attr: Tuple[AttrIntStr | Column, ...],
    values: Iterable[Tuple[Union[str, int], ...]],
    force_unnest: bool = False,
):
    ...


@overload
def in_(
    session: Session,
    attr: AttrIntStr | Column,
    values: Iterable[Union[str, int]],
    force_unnest: bool = False,
):
    ...


@overload
def in_(
    session: Session,
    attr: Union[
        AttrIntStr | Column,
        Iterable[AttrIntStr | Column],
    ],
    values: Union[Query, Selectable],
    force_unnest: Literal[False] = False,
):
    ...


def in_(
    session: Session,
    attr: Union[
        AttrIntStr | Column,
        Iterable[AttrIntStr | Column],
    ],
    values: Union[Iterable[Union[str, int, Tuple[Union[str, int], ...]]], Union[Query, Selectable]],
    force_unnest: bool = False,
):
    """
    A drop-in replacement for sqlalchemy's `in_` filter, that uses a subquery with unnest
    for performance (if length of values is above 100).

    See https://levelup.gitconnected.com/why-is-in-query-slow-on-indexed-column-in-postgresql-b716d9b563e2
    for an explanation of why this is the case.

    The following are functionally equivalent:

    ```
    select(
        Allele
    ).filter(
        in_(
            session,
            Allele.id,
            (1,2,3),
        )
    )
    ```

    and

    ```
    select(
        Genepanel
    ).filter(
        Allele.id.in_([1,2,3])
    )
    ```

    The first example is generally more efficient, as it uses a subquery with unnest, which avoids
    a sequential scan of the genepanel table for large numbers of values.

    Similarly, the following are functionally equivalent:

    ```
    select(
        Genepanel
    ).filter(
        in_(
            session,
            (Genepanel.name, Genepanel.version),
            (("panel1", "v01"), ("panel2", "v02")),
        )
    )
    ```

    and

    ```
    select(
        Genepanel
    ).filter(
        tuple_(
            Genepanel.name,
            Genepanel.version
        ).in_(
            [("panel1", "v01"), ("panel2", "v02")]
        )
    )
    ```

    """
    if isinstance(attr, Iterable):
        attr = tuple_(*attr)  # type: ignore

    assert isinstance(attr, SQLTuple) or not isinstance(attr, Iterable)

    if isinstance(values, Query):
        return attr.in_(values.selectable)  # type: ignore
    elif isinstance(values, Selectable):
        return attr.in_(values)  # type: ignore

    if not isinstance(values, (list, tuple)):
        values = list(values)

    if not values:
        # Empty list, return a filter that will never match
        return false()

    if len(values) < 100 and not force_unnest:
        # Use the default in_ filter for small lists
        return attr.in_(values)

    if isinstance(values[0], tuple) or isinstance(values[0], Row):
        assert isinstance(values[0][0], str) or isinstance(values[0][0], int)
        # map(list, zip(*values)))
        # is equivalent to
        # [[k[0] for k in values], [k[1] for k in values]],
        # for a list of tuples of length 2, but more generic
        # (assuming all tuples have the same length)
        q: Any = select(literal_column("*")).select_from(func.unnest(*map(list, zip(*values))))

        return attr.in_(q)

    assert isinstance(attr, ColumnOperators)

    cast_type: Any
    if isinstance(values[0], str):
        cast_type = String
    elif isinstance(values[0], int):
        cast_type = Integer
    else:
        raise ValueError(f"Unsupported type: {type(values[0])}")

    return attr.in_(select(func.unnest(cast(array(values), ARRAY(cast_type)))))
