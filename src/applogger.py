import logging
import logging.config
from pathlib import Path

import yaml

SCRIPT_DIR = Path(__file__).absolute().parent


def setup_logger():
    with open(SCRIPT_DIR / "logconfig.yaml", "r") as file:
        config = yaml.safe_load(file.read())
        logging.config.dictConfig(config)
