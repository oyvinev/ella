#!/usr/bin/env python
"""
Functions for setting creating migration-base database,
then running all migrations until current head.

For CI and other testing purposes.
"""
from sqlalchemy import text

from vardb.datamodel.migration.migration_base import *  # noqa: F403 # Yes, use '*'
from vardb.util import DB

from .migration_db import migration_downgrade, migration_upgrade


def ci_migration_db_remake():
    db = DB()
    db.connect()

    # Drop all tables, including alembic one...
    db.session.execute(text("DROP SCHEMA public CASCADE"))
    db.session.execute(text("CREATE SCHEMA public"))
    db.session.commit()
    Base.metadata.create_all(db.engine)  # noqa: F405


def ci_migration_upgrade_downgrade():
    ci_migration_head()
    migration_downgrade("base")


def ci_migration_head():
    ci_migration_db_remake()
    migration_upgrade("head")


def make_migration_base_db():
    db = DB()
    db.connect()
    Base.metadata.create_all(db.engine)  # noqa: F405
