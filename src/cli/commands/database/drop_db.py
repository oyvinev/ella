#!/usr/bin/env python
"""
Script for dropping all tables in a vardb database.
"""
from sqlalchemy import text

from vardb.util.db import DB


def drop_db(db: DB):
    # Drop all tables, including alembic one...
    db.session.execute(text("DROP SCHEMA public CASCADE"))
    db.session.execute(text("CREATE SCHEMA public"))
    db.session.commit()
