import datetime
import getpass
import sys
from typing import Callable

from click.testing import Result
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from cli.tests.conftest import assert_result
from vardb.datamodel import log
from vardb.util.testdatabase import TestDatabase


def test_clilog(test_database: TestDatabase, session: Session, run_command: Callable[..., Result]):
    test_database.refresh()

    result = run_command(["users", "reset_password", "testuser1"])
    assert_result(result)

    entry = session.execute(select(log.CliLog)).scalar_one()
    assert entry.user == getpass.getuser()
    assert (
        entry.output == "Reset password for user testuser1 (Ibsen, Henrik) with password *********"
    )
    assert entry.group == "users"
    assert entry.groupcommand == "reset_password"
    assert entry.reason is None
    assert entry.command == " ".join(sys.argv[1:])
    assert isinstance(entry.time, datetime.datetime)

    result = run_command(["delete", "analysis", "1"], input="Test reason\ny\n")
    assert_result(result)

    entry = session.execute(select(log.CliLog).order_by(log.CliLog.id.desc()).limit(1)).scalar_one()
    assert entry.user == getpass.getuser()
    assert entry.output == "Analysis 1 (brca_decomposed.HBOC_v1.0.0) deleted successfully"
    assert entry.group == "delete"
    assert entry.groupcommand == "analysis"
    assert entry.reason == "Test reason"
    assert entry.command == " ".join(sys.argv[1:])
    assert isinstance(entry.time, datetime.datetime)
