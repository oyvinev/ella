import os
from pathlib import Path
from typing import Any, Callable

from click.testing import Result
from sqlalchemy import func, select, tuple_
from sqlalchemy.orm.session import Session

from cli.tests.conftest import assert_result
from vardb.datamodel import assessment, gene, sample, user
from vardb.util.testdatabase import TestDatabase

TESTDATA = os.environ["TESTDATA"]
VCF = (
    Path(TESTDATA)
    / "analyses/default/brca_sample_3.HBOCUTV_v1.0.0/brca_sample_3.HBOCUTV_v1.0.0.vcf.gz"
)


def test_deposit_analysis(session: Session, run_command: Callable[..., Any]):
    # Check that analysis does not exist
    analysis_name = VCF.name[: str(VCF.name).index(".vcf")]
    assert (
        session.execute(
            select(func.count(sample.Analysis.id).filter(sample.Analysis.name == analysis_name))
        ).scalar_one()
        == 0
    )

    result = run_command(["deposit", "analysis", str(VCF)])
    assert_result(result)
    session.execute(select(sample.Analysis).filter(sample.Analysis.name == analysis_name)).one()


def test_deposit_alleles(run_command: Callable[..., Any]):
    result = run_command(["deposit", "alleles", str(VCF)])
    assert_result(result)


def test_deposit_annotation(run_command: Callable[..., Any]):
    result = run_command(["deposit", "annotation", str(VCF)])
    assert_result(result)


def test_deposit_references(session: Session, run_command: Callable[..., Any]):
    run_command(["database", "drop", "-f"])
    run_command(["database", "make", "-f"])

    assert not session.execute(func.count(assessment.Reference.id)).scalar_one()
    references = os.path.join(TESTDATA, "fixtures/references.json")
    num_references_in_file = len(open(references, "r").readlines())
    assert num_references_in_file > 0
    result = run_command(["deposit", "references", references])
    assert_result(result)
    assert (
        session.execute(select(func.count(assessment.Reference.id))).scalar_one()
        == num_references_in_file
    )


def test_deposit_genepanel(session: Session, run_command: Callable[..., Result]):
    run_command(["database", "drop", "-f"])
    run_command(["database", "make", "-f"])

    genepanel_name = "Ciliopati"
    genepanel_version = "v6.0.0"

    # Check that gene panel exists
    assert (
        session.execute(
            select(gene.Genepanel).filter(
                tuple_(gene.Genepanel.name, gene.Genepanel.version)
                == (genepanel_name, genepanel_version)
            )
        ).one_or_none()
        is None
    )

    genepanel_folder = os.path.join(
        TESTDATA, "clinicalGenePanels/{}_{}".format(genepanel_name, genepanel_version)
    )
    result = run_command(["deposit", "genepanel", "--folder", genepanel_folder])
    assert_result(result)
    # Check that gene panel exists
    session.execute(
        select(gene.Genepanel).filter(
            tuple_(gene.Genepanel.name, gene.Genepanel.version)
            == (genepanel_name, genepanel_version)
        )
    ).one()


def test_append_genepanel_to_usergroup(
    session: Session, test_database: TestDatabase, run_command: Callable[..., Any]
):
    test_database.refresh()
    genepanel_name = "Ciliopati"
    genepanel_version = "v6.0.0"
    usergroup = "testgroup01"

    ug = session.execute(
        select(user.UserGroup).filter(user.UserGroup.name == usergroup)
    ).scalar_one()
    assert (genepanel_name, genepanel_version) not in [
        (gp.name, gp.version) for gp in ug.genepanels
    ]

    result = run_command(
        ["deposit", "append_genepanel_to_usergroup", genepanel_name, genepanel_version, usergroup]
    )
    assert_result(result)

    session.refresh(ug)

    assert (genepanel_name, genepanel_version) in [(gp.name, gp.version) for gp in ug.genepanels]
