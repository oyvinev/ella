from typing import Callable, Literal

import pytest
from click.testing import Result
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from api.util.useradmin import (
    authenticate_user,
    change_password,
    generate_password,
    get_user,
    password_expired,
)
from cli.tests.conftest import assert_result
from vardb.datamodel import user
from vardb.util.testdatabase import TestDatabase


def test_user_list(run_command: Callable[..., Result]):
    result = run_command(["users", "list"])
    assert_result(result)


def test_user_activity(session: Session, run_command: Callable[..., Result]):
    result = run_command(["users", "activity"])
    assert_result(result)


@pytest.mark.parametrize("username", ["testuser1", "testuser2"])
def test_user_reset_password(
    test_database: TestDatabase,
    session: Session,
    run_command: Callable[..., Result],
    username: Literal["testuser1", "testuser2"],
):
    test_database.refresh()
    result = run_command(["users", "reset_password", username])
    assert_result(result)
    password = result.output.replace("\n", "").split(" ")[-1]
    assert password_expired(get_user(session, username))

    # Check that new password works
    new_password, _ = generate_password()
    change_password(session, username, password, new_password)
    authenticate_user(session, username, new_password)


@pytest.mark.parametrize("username", ["testuser3", "testuser4", "testuser5", "testuser6"])
def test_user_lock(
    session: Session,
    username: Literal["testuser3", "testuser4", "testuser5", "testuser6"],
    run_command: Callable[..., Result],
):
    result = run_command(["users", "lock", username])
    assert_result(result)
    session.execute(
        select(user.User).filter(user.User.username == username, user.User.active.is_(False))
    ).one()


def test_user_modify(session: Session, run_command: Callable[..., Result]):
    username = "testuser3"
    new_username = "werg3lla"
    first_name = "Jacobine Camilla"
    last_name = "Wergeland"
    email = "camilla@romantikken.no"

    result = run_command(
        [
            "users",
            "modify",
            username,
            "--new_username",
            new_username,
            "--first_name",
            first_name,
            "--last_name",
            last_name,
            "--email",
            email,
        ],
        input="y\n",
    )

    assert_result(result)

    old_user = session.execute(
        select(user.User).filter(user.User.username == username)
    ).one_or_none()
    assert old_user is None

    session.execute(
        select(user.User).filter(
            user.User.username == new_username,
            user.User.first_name == first_name,
            user.User.last_name == last_name,
            user.User.email == email,
        )
    ).one()
