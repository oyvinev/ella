"""
ELLA specific mypy plugin. Used to generate custom error messages for ELLA specific
code patterns.
"""
from mypy.errorcodes import ErrorCode
from mypy.plugin import MethodContext, Plugin
from mypy.types import AnyType, Instance, TupleType

ERROR_CODE = ErrorCode("ella-mypy", "ELLA generated mypy failure", "ELLA")


def do_not_use_method(method_name: str, alternative: str):
    "Wrapper for methods that should not be used, but has an alternative"

    def inner(att_ctx: MethodContext):
        att_ctx.api.fail(
            f"Do not use {method_name} - use {alternative} instead",
            att_ctx.context,
            code=ERROR_CODE,
        )
        return att_ctx.default_return_type

    return inner


def check_scalar_used_on_multiple(ctx: MethodContext):
    "Check if Result.scalar is used on multiple columns"

    assert isinstance(ctx.type, Instance)
    assert len(ctx.type.args) == 1
    if type(ctx.type.args[0]) is AnyType:
        return ctx.default_return_type

    assert type(ctx.type.args[0]) is TupleType
    if len(ctx.type.args[0].items) > 1:
        ctx.api.fail(
            "Multiple columns selected on, but scalars() was used - this will only return the first column",
            ctx.context,
            code=ERROR_CODE,
        )
    return ctx.default_return_type


class ELLAMypyPlugin(Plugin):
    def get_method_hook(self, fullname: str):
        if fullname == "sqlalchemy.orm.session.Session.query":
            return do_not_use_method("Session.query", "Session.execute")
        elif fullname == "sqlalchemy.orm.session.Session.get":
            return do_not_use_method("Session.get", "Session.execute")
        elif fullname in {
            "sqlalchemy.engine.result.Result.scalars",
            "sqlalchemy.engine.result.Result.scalar_one",
            "sqlalchemy.engine.result.Result.scalar_one_or_none",
        }:
            return check_scalar_used_on_multiple
        # TODO: Enable this when we are ready to fix Select.filter -> Select.where
        # elif fullname == "sqlalchemy.sql.selectable.Select.filter":
        #    return do_not_use_method("Select.filter", "Select.where")


def plugin(version: str):
    return ELLAMypyPlugin
