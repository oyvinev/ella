import { connect } from '@cerebral/angularjs'
import { props, state } from 'cerebral/tags'
import app from '../../ng-decorators'
import template from './alleleInfoSimilarAlleles.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('alleleInfoSimilarAlleles', {
    templateUrl: 'alleleInfoSimilarAlleles.ngtmpl.html',
    controller: connect({
        similar_alleles: state`views.workflows.interpretation.data.similar.${state`views.workflows.selectedAllele`}`,
        config: state`app.config.similar_alleles`
    })
})
