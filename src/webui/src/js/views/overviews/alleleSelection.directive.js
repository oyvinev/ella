import { connect } from '@cerebral/angularjs'
import { signal, state } from 'cerebral/tags'
import app from '../../ng-decorators'
import template from './alleleSelection.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('alleleSelection', {
    templateUrl: 'alleleSelection.ngtmpl.html',
    controller: connect(
        {
            alleles: state`views.overview.data.alleles`,
            finalized: state`views.overview.data.allelesFinalized`,
            state: state`views.overview.state.variants`,
            selectedSection: state`views.overview.state.selectedSection`,
            finalizedPageChanged: signal`views.overview.finalizedPageChanged`,
            collapseChanged: signal`views.overview.collapseChanged`
        },
        'AlleleSelection',
        [
            '$scope',
            ($scope) => {
                const $ctrl = $scope.$ctrl
                Object.assign($ctrl, {
                    collapseChangedWrapper(collapsed, name) {
                        $ctrl.collapseChanged({
                            section: 'variants',
                            name,
                            collapsed
                        })
                    }
                })
            }
        ]
    )
})
