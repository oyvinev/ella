import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import getGenepanel from '../actions/getGenepanel'

export default [
    when(state`app.user.group.default_import_genepanel`),
    {
        true: [
            set(props`genepanelName`, state`app.user.group.default_import_genepanel.name`),
            set(props`genepanelVersion`, state`app.user.group.default_import_genepanel.version`),
            getGenepanel,
            {
                success: [set(state`views.overview.import.data.defaultGenepanel`, props`result`)],
                error: [toast('error', 'Failed to load default genepanel')]
            }
        ],
        false: []
    }
]
