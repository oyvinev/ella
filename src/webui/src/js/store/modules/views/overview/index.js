import { Module } from 'cerebral'

import { authenticate, initApp } from '../../../common/factories'
import changeView from '../factories/changeView'
import loadOverviewState from './actions/loadOverviewState'
import redirectToSection from './actions/redirectToSection'
import setSections from './actions/setSections'
import importModule from './import'
import clearFilter from './signals/clearFilter'
import collapseChanged from './signals/collapseChanged'
import finalizedPageChanged from './signals/finalizedPageChanged'
import routed from './signals/routed'
import updateFilter from './signals/updateFilter'
import updateOverviewTriggered from './signals/updateOverviewTriggered'

const routedWithSectionSequence = initApp(authenticate([changeView('overview'), routed]))
const routedSequence = initApp(
    authenticate([changeView('overview'), setSections, loadOverviewState, redirectToSection])
)

export default Module({
    modules: {
        import: importModule
    },
    state: {}, // State set in changeView
    signals: {
        collapseChanged,
        finalizedPageChanged,
        updateOverviewTriggered,
        routedWithSection: routedWithSectionSequence,
        routed: routedSequence,
        updateFilter,
        clearFilter
    }
})
