import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import loadImportHistory from '../sequences/loadImportHistory'

export default [
    set(state`views.overview.import.importHistoryPage`, props`importHistoryPage`),
    loadImportHistory
]
