import { parallel, sequence } from 'cerebral'
import { equals, set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import interval from '../../../../common/factories/interval'
import progress from '../../../../common/factories/progress'
import toast from '../../../../common/factories/toast'
import filterAnalyses from '../actions/filterAnalyses'
import getOverviewAlleles from '../actions/getOverviewAlleles'
import getOverviewAnalyses from '../actions/getOverviewAnalyses'
import loadImport from '../import/sequences/loadImport'
import loadFinalized from '../sequences/loadFinalized'

const UPDATE_IMPORT_STATUS_INTERVAL = 30

export default sequence('loadOverview', [
    progress('start'),
    interval('stop', 'views.overview.import.updateImportJobsTriggered'),
    equals(props`section`),
    {
        // section comes from url
        variants: parallel('loadOverviewAlleles', [
            getOverviewAlleles,
            {
                success: [set(state`views.overview.data.alleles`, props`result`)],
                error: [toast('error', 'Failed to load variants')]
            },
            [set(props`page`, 1), loadFinalized]
        ]),
        analyses: parallel('loadOverviewAnalysis', [
            getOverviewAnalyses,
            {
                success: [set(state`views.overview.data.analyses`, props`result`), filterAnalyses],
                error: [toast('error', 'Failed to load analyses')]
            },
            [set(props`page`, 1), loadFinalized]
        ]),
        import: [
            // Also unset in changeView (plus above)
            interval(
                'start',
                'views.overview.import.updateImportJobsTriggered',
                {},
                UPDATE_IMPORT_STATUS_INTERVAL * 1000,
                false
            ),
            loadImport
        ],
        otherwise: [toast('error', 'Invalid section')]
    },
    progress('done')
])
