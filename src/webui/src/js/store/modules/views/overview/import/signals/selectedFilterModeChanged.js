import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import updateCandidatesFilter from '../sequences/updateCandidatesFilter'

export default [
    set(state`views.overview.import.custom.selectedFilterMode`, props`selectedFilterMode`),
    updateCandidatesFilter
]
