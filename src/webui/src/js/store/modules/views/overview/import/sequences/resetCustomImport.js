import { sequence } from 'cerebral'
import filterAndFlattenGenepanel from '../actions/filterAndFlattenGenepanel'
import prepareAddedGenepanel from '../actions/prepareAddedGenepanel'
import resetCustom from '../actions/resetCustom'

export default sequence('resetCustomImport', [
    resetCustom,
    prepareAddedGenepanel,
    // Reset added view
    filterAndFlattenGenepanel(
        'views.overview.import.custom.added.addedGenepanel',
        'views.overview.import.custom.added.filteredFlattened',
        'views.overview.import.custom.added.filter'
    )
])
