import saveOverviewState from '../actions/saveOverviewState'
import setCollapse from '../actions/setCollapse'

export default [setCollapse, saveOverviewState]
