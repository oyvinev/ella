import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'

export default [set(state`views.overview.import.importSourceType`, props`option`)]
