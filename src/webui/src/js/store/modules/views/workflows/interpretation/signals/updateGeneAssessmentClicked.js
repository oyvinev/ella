import { unset } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import loadGenepanel from '../../sequences/loadGenepanel'
import postGeneAssessment from '../actions/postGeneAssessment'

export default [
    postGeneAssessment,
    {
        success: [
            loadGenepanel,
            unset(
                state`views.workflows.interpretation.geneInformation.geneassessment.${props`geneAssessment.gene_id`}`
            )
        ],
        error: [toast('error', 'Failed to update gene information.')]
    }
]
