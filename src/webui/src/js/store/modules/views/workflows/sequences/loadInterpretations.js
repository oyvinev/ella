import { sequence } from 'cerebral'
import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../common/factories/toast'
import copyInterpretationState from '../actions/copyInterpretationState'
import getFilterConfigs from '../actions/getFilterConfigs'
import getInterpretations from '../actions/getInterpretations'
import prepareStartMode from '../actions/prepareStartMode'
import selectDefaultInterpretation from '../actions/selectDefaultInterpretation'
import loadInterpretationData from '../signals/loadInterpretationData'

export default sequence('loadInterpretations', [
    set(state`views.workflows.loaded`, false),
    getFilterConfigs,
    {
        error: [
            toast('error', props`message` ? props`message` : 'Failed to load filter configs', 30000)
        ],
        success: [
            set(state`views.workflows.data.filterconfigs`, props`result`),
            getInterpretations,
            {
                error: [toast('error', 'Failed to load interpretations', 30000)],
                success: [
                    set(state`views.workflows.data.interpretations`, props`result`),
                    selectDefaultInterpretation,
                    copyInterpretationState,

                    prepareStartMode,
                    loadInterpretationData
                ]
            }
        ]
    }
])
