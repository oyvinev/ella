import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import getAlleleIdsCategory from '../computed/getAlleleIdsCategory'
import loadAnnotationConfigs from '../sequences/loadAnnotationConfigs'
import loadExcludedAlleles from '../sequences/loadExcludedAlleles'

export default [
    set(state`views.workflows.modals.addExcludedAlleles.category`, props`category`),
    set(state`views.workflows.modals.addExcludedAlleles.selectedPage`, 1),
    set(state`views.workflows.modals.addExcludedAlleles.categoryAlleleIds`, getAlleleIdsCategory),
    loadExcludedAlleles,
    loadAnnotationConfigs
]
