import { Module } from 'cerebral'
import categoryChanged from './signals/categoryChanged'
import closeAddExcludedClicked from './signals/closeAddExcludedClicked'
import excludeAlleleClicked from './signals/excludeAlleleClicked'
import geneChanged from './signals/geneChanged'
import includeAlleleClicked from './signals/includeAlleleClicked'
import pageChanged from './signals/pageChanged'
import showAddExcludedAllelesClicked from './signals/showAddExcludedAllelesClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        showAddExcludedAllelesClicked,
        categoryChanged,
        geneChanged,
        pageChanged,
        includeAlleleClicked,
        excludeAlleleClicked,
        closeAddExcludedClicked
    }
})
