import toast from '../../../../../common/factories/toast'
import createInterpretationLog from '../actions/createInterpretationLog'
import postInterpretationLog from '../actions/postInterpretationLog'
import loadInterpretationLogs from '../sequences/loadInterpretationLogs'

export default [
    createInterpretationLog,
    postInterpretationLog,
    {
        success: [loadInterpretationLogs],
        error: [toast('error', 'Failed to clear or reinstate warning')]
    }
]
