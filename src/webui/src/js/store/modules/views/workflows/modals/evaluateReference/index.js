import { Module } from 'cerebral'
import dismissClicked from './signals/dismissClicked'
import evaluationChanged from './signals/evaluationChanged'
import showEvaluateReferenceClicked from './signals/showEvaluateReferenceClicked'
import sourcesChanged from './signals/sourcesChanged'

export default Module({
    state: {
        show: false
    },
    signals: {
        dismissClicked,
        showEvaluateReferenceClicked,
        evaluationChanged,
        sourcesChanged
    }
})
