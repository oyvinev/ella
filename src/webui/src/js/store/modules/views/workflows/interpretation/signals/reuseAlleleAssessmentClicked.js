import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import copyExistingAlleleAssessments from '../../actions/copyExistingAlleleAssessments'
import allelesChanged from '../../alleleSidebar/sequences/allelesChanged'
import autoReuseExistingReferenceAssessments from '../actions/autoReuseExistingReferenceAssessments'
import toggleReuseAlleleAssessment from '../actions/toggleReuseAlleleAssessment'
import isReadOnly from '../operators/isReadOnly'

export default [
    isReadOnly,
    {
        false: [
            toggleReuseAlleleAssessment,
            copyExistingAlleleAssessments,
            when(
                state`views.workflows.interpretation.state.allele.${props`alleleId`}.alleleassessment.reuse`
            ),
            {
                true: [],
                false: [
                    set(
                        state`views.workflows.interpretation.state.allele.${props`alleleId`}.alleleassessment.classification`,
                        null
                    )
                ]
            },
            set(
                state`views.workflows.interpretation.state.allele.${props`alleleId`}.referenceassessments`,
                []
            ),
            autoReuseExistingReferenceAssessments,
            allelesChanged
        ],
        true: []
    }
]
