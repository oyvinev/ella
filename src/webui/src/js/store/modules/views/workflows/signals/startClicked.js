import { equals, set, when } from 'cerebral/operators'
import { props, state, string } from 'cerebral/tags'
import toast from '../../../../common/factories/toast'
import startWorkflow from '../factories/startWorkflow'
import showReassignWorkflow from '../modals/reassignWorkflow/sequences/showReassignWorkflow'
import loadInterpretations from '../sequences/loadInterpretations'
import saveInterpretation from '../sequences/saveInterpretation'

// After starting the workflow, we need to reload
// all the data from backend (via loadInterpretations) to get
// correct data make sure everything is setup correctly.

const startWorkflowSequence = [
    startWorkflow('start'),
    {
        success: [loadInterpretations],
        error: [toast('error', 'Something went wrong when starting workflow.')]
    }
]

export default [
    equals(state`views.workflows.startMode`),
    {
        save: [saveInterpretation([])],
        override: [showReassignWorkflow],
        reopen: [
            startWorkflow('reopen'),
            {
                success: [
                    when(props`startWhenReopen`),
                    {
                        true: [
                            startWorkflow('start'),
                            {
                                success: [loadInterpretations],
                                error: [
                                    toast('error', 'Something went wrong when starting workflow.')
                                ]
                            }
                        ],
                        false: [loadInterpretations]
                    }
                ],
                error: [toast('error', 'Something went wrong when reopening workflow.')]
            }
        ],
        'Not ready': startWorkflowSequence,
        Interpretation: startWorkflowSequence,
        Review: startWorkflowSequence,
        'Medical review': startWorkflowSequence
    }
]
