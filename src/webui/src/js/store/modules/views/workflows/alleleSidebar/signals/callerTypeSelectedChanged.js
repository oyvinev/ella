import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import prepareSelectedAllele from '../actions/prepareSelectedAllele'
import sortSections from '../actions/sortSections'

export default [
    set(state`views.workflows.alleleSidebar.callerTypeSelected`, props`callerTypeSelected`),
    sortSections,
    prepareSelectedAllele
]
