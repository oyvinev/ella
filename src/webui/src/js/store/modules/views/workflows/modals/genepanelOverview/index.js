import { Module } from 'cerebral'
import closeClicked from './signals/closeClicked'
import copyGenesToClipboardClicked from './signals/copyGenesToClipboardClicked'
import filteredGenesPageChanged from './signals/filteredGenesPageChanged'
import geneFilterChanged from './signals/geneFilterChanged'
import showGenepanelOverviewClicked from './signals/showGenepanelOverviewClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        closeClicked,
        showGenepanelOverviewClicked,
        copyGenesToClipboardClicked,
        geneFilterChanged,
        filteredGenesPageChanged
    }
})
