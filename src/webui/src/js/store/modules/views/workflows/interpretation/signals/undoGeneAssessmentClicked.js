import { unset } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'

export default [
    unset(state`views.workflows.interpretation.geneInformation.geneassessment.${props`hgncId`}`)
]
