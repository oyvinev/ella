import { Module } from 'cerebral'
import addExcludedAlleles from './addExcludedAlleles'
import addExternal from './addExternal'
import addPrediction from './addPrediction'
import addReferences from './addReferences'
import alleleHistory from './alleleHistory'
import evaluateReference from './evaluateReference'
import finishConfirmation from './finishConfirmation'
import genepanelOverview from './genepanelOverview'
import reassignWorkflow from './reassignWorkflow'

export default Module({
    state: {
        // State would be replaced by getWorkflowState(),
        // use that if you need initial state
    },
    signals: {},
    modules: {
        addExcludedAlleles,
        alleleHistory,
        finishConfirmation,
        genepanelOverview,
        reassignWorkflow,
        addPrediction,
        addExternal,
        addReferences,
        evaluateReference
    }
})
