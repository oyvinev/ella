import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import autoIgnoreReferences from '../../interpretation/actions/autoIgnoreReferences'
import updateIgvLocus from '../../visualization/actions/updateIgvLocus'
import prepareSelectedAllele from '../actions/prepareSelectedAllele'
import sortSections from '../actions/sortSections'

// TODO: Refactor this. Currently handles more than allele sidebar stuff. Move this to workflow.

export default [
    sortSections,
    prepareSelectedAllele,
    when(state`views.workflows.selectedAllele`),
    {
        true: [set(props`alleleId`, state`views.workflows.selectedAllele`), updateIgvLocus],
        false: []
    }
]
