import { Module } from 'cerebral'
import dismissClicked from './signals/dismissClicked'
import saveClicked from './signals/saveClicked'
import selectionChanged from './signals/selectionChanged'
import showAddPredictionClicked from './signals/showAddPredictionClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        showAddPredictionClicked,
        dismissClicked,
        selectionChanged,
        saveClicked
    }
})
