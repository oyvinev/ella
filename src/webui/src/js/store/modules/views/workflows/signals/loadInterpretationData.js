import { parallel, sequence } from 'cerebral'
import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import progress from '../../../../common/factories/progress'
import toast from '../../../../common/factories/toast'
import getFilterConfig from '../actions/getFilterConfig'
import getFilteredAlleles from '../actions/getFilteredAlleles'
import setDefaultFilterConfig from '../actions/setDefaultFilterConfig'
import updateLoadingPhase from '../factories/updateLoadingPhase'
import autoIgnoreReferences from '../interpretation/actions/autoIgnoreReferences'
import loadAcmg from '../sequences/loadAcmg'
import loadAlleles from '../sequences/loadAlleles'
import loadAttachments from '../sequences/loadAttachments'
import loadGenepanel from '../sequences/loadGenepanel'
import loadReferences from '../sequences/loadReferences'

export default sequence('loadInterpretationData', [
    progress('start'),
    when(state`views.workflows.type`, (type) => type === 'analysis'),
    {
        true: [
            setDefaultFilterConfig,
            getFilterConfig,
            {
                success: [
                    set(state`views.workflows.interpretation.data.filterConfig`, props`result`)
                ],
                error: [toast('error', 'Failed to fetch filter config', 30000)]
            }
        ],
        false: []
    },
    updateLoadingPhase('filtering'),
    getFilteredAlleles,
    {
        error: [toast('error', 'Failed to fetch filtered alleles', 30000)],
        success: [
            set(state`views.workflows.interpretation.data.filteredAlleleIds`, props`result`),
            updateLoadingPhase('variants'),
            loadGenepanel,
            loadAlleles,
            progress('inc'),
            parallel([[loadReferences, loadAcmg, autoIgnoreReferences], loadAttachments]),
            updateLoadingPhase('done'),
            progress('done')
        ]
    }
])
