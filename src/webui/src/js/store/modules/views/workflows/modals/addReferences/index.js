import { Module } from 'cerebral'
import addReference from './signals/addReference'
import dismissClicked from './signals/dismissClicked'
import removeReference from './signals/removeReference'
import saveClicked from './signals/saveClicked'
import selectionChanged from './signals/selectionChanged'
import showAddReferencesClicked from './signals/showAddReferencesClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        showAddReferencesClicked,
        dismissClicked,
        selectionChanged,
        saveClicked,
        addReference,
        removeReference
    }
})
