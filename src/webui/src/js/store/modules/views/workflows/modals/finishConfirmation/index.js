import { Module } from 'cerebral'
import dismissClicked from './signals/dismissClicked'
import finishConfirmationClicked from './signals/finishConfirmationClicked'
import showFinishConfirmationClicked from './signals/showFinishConfirmationClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        showFinishConfirmationClicked,
        finishConfirmationClicked,
        dismissClicked
    }
})
