import { Module } from 'cerebral'
import dismissClicked from './signals/dismissClicked'
import reassignWorkflowClicked from './signals/reassignWorkflowClicked'

export default Module({
    state: {
        show: false
    },
    signals: {
        reassignWorkflowClicked,
        dismissClicked
    }
})
