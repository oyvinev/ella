import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import updateMessages from '../actions/updateMessages'

export default [
    set(state`views.workflows.worklog.showMessagesOnly`, props`showMessagesOnly`),
    updateMessages
]
