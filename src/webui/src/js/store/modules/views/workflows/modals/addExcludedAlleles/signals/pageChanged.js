import { debounce, set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import loadAnnotationConfigs from '../sequences/loadAnnotationConfigs'
import loadExcludedAlleles from '../sequences/loadExcludedAlleles'

export default [
    debounce(300),
    {
        continue: [
            set(state`views.workflows.modals.addExcludedAlleles.selectedPage`, props`selectedPage`),
            loadExcludedAlleles,
            loadAnnotationConfigs
        ],
        discard: []
    }
]
