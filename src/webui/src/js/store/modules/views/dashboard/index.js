import { Module } from 'cerebral'

import { authenticate, initApp } from '../../../common/factories'
import changeView from '../factories/changeView'
import logoutClicked from './signals/logoutClicked'
import routed from './signals/routed'

export default Module({
    state: {}, // State set in changeView
    signals: {
        logoutClicked,
        routed: initApp(authenticate([changeView('dashboard'), routed]))
    }
})
