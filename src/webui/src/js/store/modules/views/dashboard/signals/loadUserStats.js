import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../common/factories/toast'
import getUserStats from '../actions/getUserStats'

export default [
    getUserStats,
    {
        success: [set(state`views.dashboard.data.userStats`, props`result`)],
        error: [toast('error', "Failed finding user's work statistics")]
    }
]
