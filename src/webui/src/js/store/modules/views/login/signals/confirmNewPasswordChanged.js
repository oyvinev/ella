import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import checkConfirmPassword from '../actions/checkConfirmPassword'

export default [
    set(state`views.login.confirmNewPassword`, props`confirmNewPassword`),
    checkConfirmPassword
]
