import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import getAnalysesForAllele from '../actions/getAnalysesForAllele'

export default [
    set(state`search.modals.showAnalysesForAllele.warningAccepted`, true),
    getAnalysesForAllele,
    {
        success: [set(state`search.modals.showAnalysesForAllele.data.analyses`, props`result`)],
        error: [toast('error', 'An error occured while fetching analyses.')]
    }
]
