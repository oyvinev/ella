import { set } from 'cerebral/operators'
import { props, state, string } from 'cerebral/tags'

export default [
    set(state`search.modals.showAnalysesForAllele.allele`, props`allele`),
    set(state`search.modals.showAnalysesForAllele.show`, true)
]
