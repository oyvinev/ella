#!/bin/bash -ue

# Check if database migration is complete - this is done using alembic
# and `alembic check` will return non-zero if `alembic --autogenerate`
# would generate any changes.

echo "Starting up postgres"
./ops/common/common_pg_startup init &>/dev/null &
make dbsleep

ella-cli database ci-migration-head -f
pushd /ella/src/vardb/datamodel/migration
alembic check
popd
