# Demo

To have a quick look at ELLA and how it works, you can easily bring up a demo instance if you have
Docker installed. The database is pre-loaded with analyses and variants and users with different
roles and configurations. Details on the test data are available in the
[alleles/ella-testdata](https://gitlab.com/alleles/ella-testdata.git) repo.

<!-- Is this still true? -->
::: warning NOTE
The demo instance lacks an annotation service and does not have data import functionality.
:::

## Starting the demo instance


```bash
# use the latest tagged release
make build && make dev
```
Once the demo is started, navigate to http://localhost:5000 in your browser. You should see the
login page.

### Users

There are several test users `testuser1`...`testuser8`, all with password `demo`. These are placed
in different user groups, with access to different samples, UI setups and filter configurations:

| Group | Users                   | Access to                                |
| :---- | :---------------------- | :--------------------------------------- |
| 1     | `testuser1`–`testuser3` | Small gene panels, VARIANTS and ANALYSES |
| 2     | `testuser4`–`testuser6` | Large gene panels, ANALYSES only         |
| 3     | `testuser7`–`testuser8` | All gene panels, ANALYSES only           |

## Stop demo

To stop the demo instance, run:

``` bash
make kill
```
